;; Auto-generated. Do not edit!


(when (boundp 'ur_application::pop)
  (if (not (find-package "UR_APPLICATION"))
    (make-package "UR_APPLICATION"))
  (shadow 'pop (find-package "UR_APPLICATION")))
(unless (find-package "UR_APPLICATION::POP")
  (make-package "UR_APPLICATION::POP"))

(in-package "ROS")
;;//! \htmlinclude pop.msg.html


(defclass ur_application::pop
  :super ros::object
  :slots (_pop ))

(defmethod ur_application::pop
  (:init
   (&key
    ((:pop __pop) 0.0)
    )
   (send-super :init)
   (setq _pop (float __pop))
   self)
  (:pop
   (&optional __pop)
   (if __pop (setq _pop __pop)) _pop)
  (:serialization-length
   ()
   (+
    ;; float64 _pop
    8
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float64 _pop
       (sys::poke _pop (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float64 _pop
     (setq _pop (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;;
   self)
  )

(setf (get ur_application::pop :md5sum-) "4e628c2409f06bc376c6f920be84cf66")
(setf (get ur_application::pop :datatype-) "ur_application/pop")
(setf (get ur_application::pop :definition-)
      "float64 pop

")



(provide :ur_application/pop "4e628c2409f06bc376c6f920be84cf66")


