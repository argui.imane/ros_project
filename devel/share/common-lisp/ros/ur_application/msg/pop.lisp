; Auto-generated. Do not edit!


(cl:in-package ur_application-msg)


;//! \htmlinclude pop.msg.html

(cl:defclass <pop> (roslisp-msg-protocol:ros-message)
  ((pop
    :reader pop
    :initarg :pop
    :type cl:float
    :initform 0.0))
)

(cl:defclass pop (<pop>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <pop>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'pop)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name ur_application-msg:<pop> is deprecated: use ur_application-msg:pop instead.")))

(cl:ensure-generic-function 'pop-val :lambda-list '(m))
(cl:defmethod pop-val ((m <pop>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader ur_application-msg:pop-val is deprecated.  Use ur_application-msg:pop instead.")
  (pop m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <pop>) ostream)
  "Serializes a message object of type '<pop>"
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'pop))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <pop>) istream)
  "Deserializes a message object of type '<pop>"
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'pop) (roslisp-utils:decode-double-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<pop>)))
  "Returns string type for a message object of type '<pop>"
  "ur_application/pop")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'pop)))
  "Returns string type for a message object of type 'pop"
  "ur_application/pop")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<pop>)))
  "Returns md5sum for a message object of type '<pop>"
  "4e628c2409f06bc376c6f920be84cf66")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'pop)))
  "Returns md5sum for a message object of type 'pop"
  "4e628c2409f06bc376c6f920be84cf66")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<pop>)))
  "Returns full string definition for message of type '<pop>"
  (cl:format cl:nil "float64 pop~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'pop)))
  "Returns full string definition for message of type 'pop"
  (cl:format cl:nil "float64 pop~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <pop>))
  (cl:+ 0
     8
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <pop>))
  "Converts a ROS message object to a list"
  (cl:list 'pop
    (cl:cons ':pop (pop msg))
))
