
(cl:in-package :asdf)

(defsystem "ur_application-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "Consigne" :depends-on ("_package_Consigne"))
    (:file "_package_Consigne" :depends-on ("_package"))
  ))