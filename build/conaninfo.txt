[settings]
    arch=x86_64
    build_type=Release
    compiler=gcc
    compiler.libcxx=libstdc++11
    compiler.version=7
    os=Linux

[requires]
    robot-control/1.Y.Z
    robot-model/1.Y.Z
    robot-trajectory/1.Y.Z

[options]


[full_settings]
    arch=x86_64
    arch_build=x86_64
    build_type=Release
    compiler=gcc
    compiler.libcxx=libstdc++11
    compiler.version=7
    os=Linux
    os_build=Linux

[full_requires]
    ecos/2.0.7@bnavarro/stable:9bfdcfa2bb925892ecf42e2a018a3f3529826676
    eigen/3.3.7@conan/stable:5ab84d6acfe1f23c4fae0ab88f26e3a396351ac9
    eigen-extensions/0.13.2@bnavarro/testing:5ab84d6acfe1f23c4fae0ab88f26e3a396351ac9
    epigraph/0.4.0@bnavarro/stable:1d7ac1c9c1daee8278f56b35a8bf95277d9fad5c
    fmt/7.0.1:66c5327ebdcecae0a01a863939964495fa019a06
    osqp/0.6.0@bnavarro/stable:9bfdcfa2bb925892ecf42e2a018a3f3529826676
    robot-control/1.0@bnavarro/testing:22e5a59e91a5280d237198bd58f1bba2144479b2
    robot-model/1.0@bnavarro/testing:7ebec6d21ff02bfaccef00531a647309ae998255
    robot-trajectory/1.0@bnavarro/testing:d6b96dcf702dae5b449f2dc4f8e363f049487c4b
    tinyxml2/8.0.0:66c5327ebdcecae0a01a863939964495fa019a06
    urdfdomcpp/1.0@bnavarro/stable:c62e1023a11546ac53345798073dd54f92db8e1c

[full_options]
    epigraph:enable_ecos=True
    epigraph:enable_osqp=True
    epigraph:fPIC=True
    fmt:fPIC=True
    fmt:header_only=False
    fmt:shared=False
    fmt:with_fmt_alias=False
    robot-control:build_tests=False
    robot-control:fPIC=True
    robot-control:shared=False
    robot-model:build_tests=False
    robot-model:fPIC=True
    robot-model:shared=False
    robot-trajectory:build_tests=False
    robot-trajectory:fPIC=True
    robot-trajectory:shared=False
    tinyxml2:fPIC=True
    tinyxml2:shared=False
    urdfdomcpp:build_tests=False
    urdfdomcpp:fPIC=True
    urdfdomcpp:shared=False

[recipe_hash]


[env]

