# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/hamza/catkin_ws/src/ur_application/src/ur_application/main.cpp" "/home/hamza/catkin_ws/build/ur_application/CMakeFiles/ur_application_ur_application.dir/src/ur_application/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "CTRLC=1"
  "DLONG"
  "EIGEN_DENSEBASE_PLUGIN=<Eigen/dense_base_extensions.h>"
  "EIGEN_QUATERNIONBASE_PLUGIN=<Eigen/quaternion_base_extensions.h>"
  "ENABLE_ECOS"
  "ENABLE_OSQP"
  "LDL_LONG"
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_BUILD_SHARED_LIBS=1"
  "ROS_PACKAGE_NAME=\"ur_application\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/hamza/catkin_ws/devel/include"
  "/opt/ros/melodic/include"
  "/opt/ros/melodic/share/xmlrpcpp/cmake/../../../include/xmlrpcpp"
  "/home/hamza/catkin_ws/src/ur_application/include"
  "/home/hamza/.conan/data/robot-control/1.0/bnavarro/testing/package/22e5a59e91a5280d237198bd58f1bba2144479b2/include"
  "/home/hamza/.conan/data/fmt/7.0.1/_/_/package/66c5327ebdcecae0a01a863939964495fa019a06/include"
  "/home/hamza/.conan/data/robot-model/1.0/bnavarro/testing/package/7ebec6d21ff02bfaccef00531a647309ae998255/include"
  "/home/hamza/.conan/data/urdfdomcpp/1.0/bnavarro/stable/package/c62e1023a11546ac53345798073dd54f92db8e1c/include"
  "/home/hamza/.conan/data/tinyxml2/8.0.0/_/_/package/66c5327ebdcecae0a01a863939964495fa019a06/include"
  "/home/hamza/.conan/data/eigen/3.3.7/conan/stable/package/5ab84d6acfe1f23c4fae0ab88f26e3a396351ac9/include/eigen3"
  "/home/hamza/.conan/data/eigen-extensions/0.13.2/bnavarro/testing/package/5ab84d6acfe1f23c4fae0ab88f26e3a396351ac9/include"
  "/home/hamza/.conan/data/epigraph/0.4.0/bnavarro/stable/package/1d7ac1c9c1daee8278f56b35a8bf95277d9fad5c/include"
  "/home/hamza/.conan/data/osqp/0.6.0/bnavarro/stable/package/9bfdcfa2bb925892ecf42e2a018a3f3529826676/include/osqp"
  "/home/hamza/.conan/data/osqp/0.6.0/bnavarro/stable/package/9bfdcfa2bb925892ecf42e2a018a3f3529826676/include/qdldl"
  "/home/hamza/.conan/data/ecos/2.0.7/bnavarro/stable/package/9bfdcfa2bb925892ecf42e2a018a3f3529826676/include/ecos"
  "/home/hamza/.conan/data/ecos/2.0.7/bnavarro/stable/package/9bfdcfa2bb925892ecf42e2a018a3f3529826676/include/SuiteSparse_config"
  "/home/hamza/.conan/data/robot-trajectory/1.0/bnavarro/testing/package/d6b96dcf702dae5b449f2dc4f8e363f049487c4b/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
