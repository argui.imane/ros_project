include(CMakeParseArguments)

macro(conan_find_apple_frameworks FRAMEWORKS_FOUND FRAMEWORKS SUFFIX BUILD_TYPE)
    if(APPLE)
        if(CMAKE_BUILD_TYPE)
            set(_BTYPE ${CMAKE_BUILD_TYPE})
        elseif(NOT BUILD_TYPE STREQUAL "")
            set(_BTYPE ${BUILD_TYPE})
        endif()
        if(_BTYPE)
            if(${_BTYPE} MATCHES "Debug|_DEBUG")
                set(CONAN_FRAMEWORKS${SUFFIX} ${CONAN_FRAMEWORKS${SUFFIX}_DEBUG} ${CONAN_FRAMEWORKS${SUFFIX}})
                set(CONAN_FRAMEWORK_DIRS${SUFFIX} ${CONAN_FRAMEWORK_DIRS${SUFFIX}_DEBUG} ${CONAN_FRAMEWORK_DIRS${SUFFIX}})
            elseif(${_BTYPE} MATCHES "Release|_RELEASE")
                set(CONAN_FRAMEWORKS${SUFFIX} ${CONAN_FRAMEWORKS${SUFFIX}_RELEASE} ${CONAN_FRAMEWORKS${SUFFIX}})
                set(CONAN_FRAMEWORK_DIRS${SUFFIX} ${CONAN_FRAMEWORK_DIRS${SUFFIX}_RELEASE} ${CONAN_FRAMEWORK_DIRS${SUFFIX}})
            elseif(${_BTYPE} MATCHES "RelWithDebInfo|_RELWITHDEBINFO")
                set(CONAN_FRAMEWORKS${SUFFIX} ${CONAN_FRAMEWORKS${SUFFIX}_RELWITHDEBINFO} ${CONAN_FRAMEWORKS${SUFFIX}})
                set(CONAN_FRAMEWORK_DIRS${SUFFIX} ${CONAN_FRAMEWORK_DIRS${SUFFIX}_RELWITHDEBINFO} ${CONAN_FRAMEWORK_DIRS${SUFFIX}})
            elseif(${_BTYPE} MATCHES "MinSizeRel|_MINSIZEREL")
                set(CONAN_FRAMEWORKS${SUFFIX} ${CONAN_FRAMEWORKS${SUFFIX}_MINSIZEREL} ${CONAN_FRAMEWORKS${SUFFIX}})
                set(CONAN_FRAMEWORK_DIRS${SUFFIX} ${CONAN_FRAMEWORK_DIRS${SUFFIX}_MINSIZEREL} ${CONAN_FRAMEWORK_DIRS${SUFFIX}})
            endif()
        endif()
        foreach(_FRAMEWORK ${FRAMEWORKS})
            # https://cmake.org/pipermail/cmake-developers/2017-August/030199.html
            find_library(CONAN_FRAMEWORK_${_FRAMEWORK}_FOUND NAME ${_FRAMEWORK} PATHS ${CONAN_FRAMEWORK_DIRS${SUFFIX}} CMAKE_FIND_ROOT_PATH_BOTH)
            if(CONAN_FRAMEWORK_${_FRAMEWORK}_FOUND)
                list(APPEND ${FRAMEWORKS_FOUND} ${CONAN_FRAMEWORK_${_FRAMEWORK}_FOUND})
            else()
                message(FATAL_ERROR "Framework library ${_FRAMEWORK} not found in paths: ${CONAN_FRAMEWORK_DIRS${SUFFIX}}")
            endif()
        endforeach()
    endif()
endmacro()


#################
###  ROBOT-TRAJECTORY
#################
set(CONAN_ROBOT-TRAJECTORY_ROOT "/home/hamza/.conan/data/robot-trajectory/1.0/bnavarro/testing/package/d6b96dcf702dae5b449f2dc4f8e363f049487c4b")
set(CONAN_INCLUDE_DIRS_ROBOT-TRAJECTORY "/home/hamza/.conan/data/robot-trajectory/1.0/bnavarro/testing/package/d6b96dcf702dae5b449f2dc4f8e363f049487c4b/include")
set(CONAN_LIB_DIRS_ROBOT-TRAJECTORY "/home/hamza/.conan/data/robot-trajectory/1.0/bnavarro/testing/package/d6b96dcf702dae5b449f2dc4f8e363f049487c4b/lib")
set(CONAN_BIN_DIRS_ROBOT-TRAJECTORY "/home/hamza/.conan/data/robot-trajectory/1.0/bnavarro/testing/package/d6b96dcf702dae5b449f2dc4f8e363f049487c4b/bin")
set(CONAN_RES_DIRS_ROBOT-TRAJECTORY )
set(CONAN_SRC_DIRS_ROBOT-TRAJECTORY )
set(CONAN_BUILD_DIRS_ROBOT-TRAJECTORY "/home/hamza/.conan/data/robot-trajectory/1.0/bnavarro/testing/package/d6b96dcf702dae5b449f2dc4f8e363f049487c4b/")
set(CONAN_FRAMEWORK_DIRS_ROBOT-TRAJECTORY )
set(CONAN_LIBS_ROBOT-TRAJECTORY robot_trajectory)
set(CONAN_PKG_LIBS_ROBOT-TRAJECTORY robot_trajectory)
set(CONAN_SYSTEM_LIBS_ROBOT-TRAJECTORY )
set(CONAN_FRAMEWORKS_ROBOT-TRAJECTORY )
set(CONAN_FRAMEWORKS_FOUND_ROBOT-TRAJECTORY "")  # Will be filled later
set(CONAN_DEFINES_ROBOT-TRAJECTORY )
set(CONAN_BUILD_MODULES_PATHS_ROBOT-TRAJECTORY )
# COMPILE_DEFINITIONS are equal to CONAN_DEFINES without -D, for targets
set(CONAN_COMPILE_DEFINITIONS_ROBOT-TRAJECTORY )

set(CONAN_C_FLAGS_ROBOT-TRAJECTORY "")
set(CONAN_CXX_FLAGS_ROBOT-TRAJECTORY "")
set(CONAN_SHARED_LINKER_FLAGS_ROBOT-TRAJECTORY "")
set(CONAN_EXE_LINKER_FLAGS_ROBOT-TRAJECTORY "")

# For modern cmake targets we use the list variables (separated with ;)
set(CONAN_C_FLAGS_ROBOT-TRAJECTORY_LIST "")
set(CONAN_CXX_FLAGS_ROBOT-TRAJECTORY_LIST "")
set(CONAN_SHARED_LINKER_FLAGS_ROBOT-TRAJECTORY_LIST "")
set(CONAN_EXE_LINKER_FLAGS_ROBOT-TRAJECTORY_LIST "")

# Apple Frameworks
conan_find_apple_frameworks(CONAN_FRAMEWORKS_FOUND_ROBOT-TRAJECTORY "${CONAN_FRAMEWORKS_ROBOT-TRAJECTORY}" "_ROBOT-TRAJECTORY" "")
# Append to aggregated values variable
set(CONAN_LIBS_ROBOT-TRAJECTORY ${CONAN_PKG_LIBS_ROBOT-TRAJECTORY} ${CONAN_SYSTEM_LIBS_ROBOT-TRAJECTORY} ${CONAN_FRAMEWORKS_FOUND_ROBOT-TRAJECTORY})


#################
###  ROBOT-CONTROL
#################
set(CONAN_ROBOT-CONTROL_ROOT "/home/hamza/.conan/data/robot-control/1.0/bnavarro/testing/package/22e5a59e91a5280d237198bd58f1bba2144479b2")
set(CONAN_INCLUDE_DIRS_ROBOT-CONTROL "/home/hamza/.conan/data/robot-control/1.0/bnavarro/testing/package/22e5a59e91a5280d237198bd58f1bba2144479b2/include")
set(CONAN_LIB_DIRS_ROBOT-CONTROL "/home/hamza/.conan/data/robot-control/1.0/bnavarro/testing/package/22e5a59e91a5280d237198bd58f1bba2144479b2/lib")
set(CONAN_BIN_DIRS_ROBOT-CONTROL "/home/hamza/.conan/data/robot-control/1.0/bnavarro/testing/package/22e5a59e91a5280d237198bd58f1bba2144479b2/bin")
set(CONAN_RES_DIRS_ROBOT-CONTROL )
set(CONAN_SRC_DIRS_ROBOT-CONTROL )
set(CONAN_BUILD_DIRS_ROBOT-CONTROL "/home/hamza/.conan/data/robot-control/1.0/bnavarro/testing/package/22e5a59e91a5280d237198bd58f1bba2144479b2/")
set(CONAN_FRAMEWORK_DIRS_ROBOT-CONTROL )
set(CONAN_LIBS_ROBOT-CONTROL robot_control)
set(CONAN_PKG_LIBS_ROBOT-CONTROL robot_control)
set(CONAN_SYSTEM_LIBS_ROBOT-CONTROL )
set(CONAN_FRAMEWORKS_ROBOT-CONTROL )
set(CONAN_FRAMEWORKS_FOUND_ROBOT-CONTROL "")  # Will be filled later
set(CONAN_DEFINES_ROBOT-CONTROL )
set(CONAN_BUILD_MODULES_PATHS_ROBOT-CONTROL )
# COMPILE_DEFINITIONS are equal to CONAN_DEFINES without -D, for targets
set(CONAN_COMPILE_DEFINITIONS_ROBOT-CONTROL )

set(CONAN_C_FLAGS_ROBOT-CONTROL "")
set(CONAN_CXX_FLAGS_ROBOT-CONTROL "")
set(CONAN_SHARED_LINKER_FLAGS_ROBOT-CONTROL "")
set(CONAN_EXE_LINKER_FLAGS_ROBOT-CONTROL "")

# For modern cmake targets we use the list variables (separated with ;)
set(CONAN_C_FLAGS_ROBOT-CONTROL_LIST "")
set(CONAN_CXX_FLAGS_ROBOT-CONTROL_LIST "")
set(CONAN_SHARED_LINKER_FLAGS_ROBOT-CONTROL_LIST "")
set(CONAN_EXE_LINKER_FLAGS_ROBOT-CONTROL_LIST "")

# Apple Frameworks
conan_find_apple_frameworks(CONAN_FRAMEWORKS_FOUND_ROBOT-CONTROL "${CONAN_FRAMEWORKS_ROBOT-CONTROL}" "_ROBOT-CONTROL" "")
# Append to aggregated values variable
set(CONAN_LIBS_ROBOT-CONTROL ${CONAN_PKG_LIBS_ROBOT-CONTROL} ${CONAN_SYSTEM_LIBS_ROBOT-CONTROL} ${CONAN_FRAMEWORKS_FOUND_ROBOT-CONTROL})


#################
###  ROBOT-MODEL
#################
set(CONAN_ROBOT-MODEL_ROOT "/home/hamza/.conan/data/robot-model/1.0/bnavarro/testing/package/7ebec6d21ff02bfaccef00531a647309ae998255")
set(CONAN_INCLUDE_DIRS_ROBOT-MODEL "/home/hamza/.conan/data/robot-model/1.0/bnavarro/testing/package/7ebec6d21ff02bfaccef00531a647309ae998255/include")
set(CONAN_LIB_DIRS_ROBOT-MODEL "/home/hamza/.conan/data/robot-model/1.0/bnavarro/testing/package/7ebec6d21ff02bfaccef00531a647309ae998255/lib")
set(CONAN_BIN_DIRS_ROBOT-MODEL "/home/hamza/.conan/data/robot-model/1.0/bnavarro/testing/package/7ebec6d21ff02bfaccef00531a647309ae998255/bin")
set(CONAN_RES_DIRS_ROBOT-MODEL )
set(CONAN_SRC_DIRS_ROBOT-MODEL )
set(CONAN_BUILD_DIRS_ROBOT-MODEL "/home/hamza/.conan/data/robot-model/1.0/bnavarro/testing/package/7ebec6d21ff02bfaccef00531a647309ae998255/")
set(CONAN_FRAMEWORK_DIRS_ROBOT-MODEL )
set(CONAN_LIBS_ROBOT-MODEL robot_model)
set(CONAN_PKG_LIBS_ROBOT-MODEL robot_model)
set(CONAN_SYSTEM_LIBS_ROBOT-MODEL )
set(CONAN_FRAMEWORKS_ROBOT-MODEL )
set(CONAN_FRAMEWORKS_FOUND_ROBOT-MODEL "")  # Will be filled later
set(CONAN_DEFINES_ROBOT-MODEL )
set(CONAN_BUILD_MODULES_PATHS_ROBOT-MODEL )
# COMPILE_DEFINITIONS are equal to CONAN_DEFINES without -D, for targets
set(CONAN_COMPILE_DEFINITIONS_ROBOT-MODEL )

set(CONAN_C_FLAGS_ROBOT-MODEL "")
set(CONAN_CXX_FLAGS_ROBOT-MODEL "")
set(CONAN_SHARED_LINKER_FLAGS_ROBOT-MODEL "")
set(CONAN_EXE_LINKER_FLAGS_ROBOT-MODEL "")

# For modern cmake targets we use the list variables (separated with ;)
set(CONAN_C_FLAGS_ROBOT-MODEL_LIST "")
set(CONAN_CXX_FLAGS_ROBOT-MODEL_LIST "")
set(CONAN_SHARED_LINKER_FLAGS_ROBOT-MODEL_LIST "")
set(CONAN_EXE_LINKER_FLAGS_ROBOT-MODEL_LIST "")

# Apple Frameworks
conan_find_apple_frameworks(CONAN_FRAMEWORKS_FOUND_ROBOT-MODEL "${CONAN_FRAMEWORKS_ROBOT-MODEL}" "_ROBOT-MODEL" "")
# Append to aggregated values variable
set(CONAN_LIBS_ROBOT-MODEL ${CONAN_PKG_LIBS_ROBOT-MODEL} ${CONAN_SYSTEM_LIBS_ROBOT-MODEL} ${CONAN_FRAMEWORKS_FOUND_ROBOT-MODEL})


#################
###  EIGEN-EXTENSIONS
#################
set(CONAN_EIGEN-EXTENSIONS_ROOT "/home/hamza/.conan/data/eigen-extensions/0.13.2/bnavarro/testing/package/5ab84d6acfe1f23c4fae0ab88f26e3a396351ac9")
set(CONAN_INCLUDE_DIRS_EIGEN-EXTENSIONS "/home/hamza/.conan/data/eigen-extensions/0.13.2/bnavarro/testing/package/5ab84d6acfe1f23c4fae0ab88f26e3a396351ac9/include")
set(CONAN_LIB_DIRS_EIGEN-EXTENSIONS )
set(CONAN_BIN_DIRS_EIGEN-EXTENSIONS )
set(CONAN_RES_DIRS_EIGEN-EXTENSIONS )
set(CONAN_SRC_DIRS_EIGEN-EXTENSIONS )
set(CONAN_BUILD_DIRS_EIGEN-EXTENSIONS "/home/hamza/.conan/data/eigen-extensions/0.13.2/bnavarro/testing/package/5ab84d6acfe1f23c4fae0ab88f26e3a396351ac9/")
set(CONAN_FRAMEWORK_DIRS_EIGEN-EXTENSIONS )
set(CONAN_LIBS_EIGEN-EXTENSIONS )
set(CONAN_PKG_LIBS_EIGEN-EXTENSIONS )
set(CONAN_SYSTEM_LIBS_EIGEN-EXTENSIONS )
set(CONAN_FRAMEWORKS_EIGEN-EXTENSIONS )
set(CONAN_FRAMEWORKS_FOUND_EIGEN-EXTENSIONS "")  # Will be filled later
set(CONAN_DEFINES_EIGEN-EXTENSIONS "-DEIGEN_DENSEBASE_PLUGIN=<Eigen/dense_base_extensions.h>"
			"-DEIGEN_QUATERNIONBASE_PLUGIN=<Eigen/quaternion_base_extensions.h>")
set(CONAN_BUILD_MODULES_PATHS_EIGEN-EXTENSIONS )
# COMPILE_DEFINITIONS are equal to CONAN_DEFINES without -D, for targets
set(CONAN_COMPILE_DEFINITIONS_EIGEN-EXTENSIONS "EIGEN_DENSEBASE_PLUGIN=<Eigen/dense_base_extensions.h>"
			"EIGEN_QUATERNIONBASE_PLUGIN=<Eigen/quaternion_base_extensions.h>")

set(CONAN_C_FLAGS_EIGEN-EXTENSIONS "")
set(CONAN_CXX_FLAGS_EIGEN-EXTENSIONS "")
set(CONAN_SHARED_LINKER_FLAGS_EIGEN-EXTENSIONS "")
set(CONAN_EXE_LINKER_FLAGS_EIGEN-EXTENSIONS "")

# For modern cmake targets we use the list variables (separated with ;)
set(CONAN_C_FLAGS_EIGEN-EXTENSIONS_LIST "")
set(CONAN_CXX_FLAGS_EIGEN-EXTENSIONS_LIST "")
set(CONAN_SHARED_LINKER_FLAGS_EIGEN-EXTENSIONS_LIST "")
set(CONAN_EXE_LINKER_FLAGS_EIGEN-EXTENSIONS_LIST "")

# Apple Frameworks
conan_find_apple_frameworks(CONAN_FRAMEWORKS_FOUND_EIGEN-EXTENSIONS "${CONAN_FRAMEWORKS_EIGEN-EXTENSIONS}" "_EIGEN-EXTENSIONS" "")
# Append to aggregated values variable
set(CONAN_LIBS_EIGEN-EXTENSIONS ${CONAN_PKG_LIBS_EIGEN-EXTENSIONS} ${CONAN_SYSTEM_LIBS_EIGEN-EXTENSIONS} ${CONAN_FRAMEWORKS_FOUND_EIGEN-EXTENSIONS})


#################
###  EPIGRAPH
#################
set(CONAN_EPIGRAPH_ROOT "/home/hamza/.conan/data/epigraph/0.4.0/bnavarro/stable/package/1d7ac1c9c1daee8278f56b35a8bf95277d9fad5c")
set(CONAN_INCLUDE_DIRS_EPIGRAPH "/home/hamza/.conan/data/epigraph/0.4.0/bnavarro/stable/package/1d7ac1c9c1daee8278f56b35a8bf95277d9fad5c/include")
set(CONAN_LIB_DIRS_EPIGRAPH "/home/hamza/.conan/data/epigraph/0.4.0/bnavarro/stable/package/1d7ac1c9c1daee8278f56b35a8bf95277d9fad5c/lib")
set(CONAN_BIN_DIRS_EPIGRAPH )
set(CONAN_RES_DIRS_EPIGRAPH )
set(CONAN_SRC_DIRS_EPIGRAPH )
set(CONAN_BUILD_DIRS_EPIGRAPH "/home/hamza/.conan/data/epigraph/0.4.0/bnavarro/stable/package/1d7ac1c9c1daee8278f56b35a8bf95277d9fad5c/")
set(CONAN_FRAMEWORK_DIRS_EPIGRAPH )
set(CONAN_LIBS_EPIGRAPH epigraph)
set(CONAN_PKG_LIBS_EPIGRAPH epigraph)
set(CONAN_SYSTEM_LIBS_EPIGRAPH )
set(CONAN_FRAMEWORKS_EPIGRAPH )
set(CONAN_FRAMEWORKS_FOUND_EPIGRAPH "")  # Will be filled later
set(CONAN_DEFINES_EPIGRAPH "-DENABLE_OSQP"
			"-DENABLE_ECOS")
set(CONAN_BUILD_MODULES_PATHS_EPIGRAPH )
# COMPILE_DEFINITIONS are equal to CONAN_DEFINES without -D, for targets
set(CONAN_COMPILE_DEFINITIONS_EPIGRAPH "ENABLE_OSQP"
			"ENABLE_ECOS")

set(CONAN_C_FLAGS_EPIGRAPH "")
set(CONAN_CXX_FLAGS_EPIGRAPH "")
set(CONAN_SHARED_LINKER_FLAGS_EPIGRAPH "")
set(CONAN_EXE_LINKER_FLAGS_EPIGRAPH "")

# For modern cmake targets we use the list variables (separated with ;)
set(CONAN_C_FLAGS_EPIGRAPH_LIST "")
set(CONAN_CXX_FLAGS_EPIGRAPH_LIST "")
set(CONAN_SHARED_LINKER_FLAGS_EPIGRAPH_LIST "")
set(CONAN_EXE_LINKER_FLAGS_EPIGRAPH_LIST "")

# Apple Frameworks
conan_find_apple_frameworks(CONAN_FRAMEWORKS_FOUND_EPIGRAPH "${CONAN_FRAMEWORKS_EPIGRAPH}" "_EPIGRAPH" "")
# Append to aggregated values variable
set(CONAN_LIBS_EPIGRAPH ${CONAN_PKG_LIBS_EPIGRAPH} ${CONAN_SYSTEM_LIBS_EPIGRAPH} ${CONAN_FRAMEWORKS_FOUND_EPIGRAPH})


#################
###  URDFDOMCPP
#################
set(CONAN_URDFDOMCPP_ROOT "/home/hamza/.conan/data/urdfdomcpp/1.0/bnavarro/stable/package/c62e1023a11546ac53345798073dd54f92db8e1c")
set(CONAN_INCLUDE_DIRS_URDFDOMCPP "/home/hamza/.conan/data/urdfdomcpp/1.0/bnavarro/stable/package/c62e1023a11546ac53345798073dd54f92db8e1c/include")
set(CONAN_LIB_DIRS_URDFDOMCPP "/home/hamza/.conan/data/urdfdomcpp/1.0/bnavarro/stable/package/c62e1023a11546ac53345798073dd54f92db8e1c/lib")
set(CONAN_BIN_DIRS_URDFDOMCPP "/home/hamza/.conan/data/urdfdomcpp/1.0/bnavarro/stable/package/c62e1023a11546ac53345798073dd54f92db8e1c/bin")
set(CONAN_RES_DIRS_URDFDOMCPP )
set(CONAN_SRC_DIRS_URDFDOMCPP )
set(CONAN_BUILD_DIRS_URDFDOMCPP "/home/hamza/.conan/data/urdfdomcpp/1.0/bnavarro/stable/package/c62e1023a11546ac53345798073dd54f92db8e1c/")
set(CONAN_FRAMEWORK_DIRS_URDFDOMCPP )
set(CONAN_LIBS_URDFDOMCPP urdfdomcpp)
set(CONAN_PKG_LIBS_URDFDOMCPP urdfdomcpp)
set(CONAN_SYSTEM_LIBS_URDFDOMCPP )
set(CONAN_FRAMEWORKS_URDFDOMCPP )
set(CONAN_FRAMEWORKS_FOUND_URDFDOMCPP "")  # Will be filled later
set(CONAN_DEFINES_URDFDOMCPP )
set(CONAN_BUILD_MODULES_PATHS_URDFDOMCPP )
# COMPILE_DEFINITIONS are equal to CONAN_DEFINES without -D, for targets
set(CONAN_COMPILE_DEFINITIONS_URDFDOMCPP )

set(CONAN_C_FLAGS_URDFDOMCPP "")
set(CONAN_CXX_FLAGS_URDFDOMCPP "")
set(CONAN_SHARED_LINKER_FLAGS_URDFDOMCPP "")
set(CONAN_EXE_LINKER_FLAGS_URDFDOMCPP "")

# For modern cmake targets we use the list variables (separated with ;)
set(CONAN_C_FLAGS_URDFDOMCPP_LIST "")
set(CONAN_CXX_FLAGS_URDFDOMCPP_LIST "")
set(CONAN_SHARED_LINKER_FLAGS_URDFDOMCPP_LIST "")
set(CONAN_EXE_LINKER_FLAGS_URDFDOMCPP_LIST "")

# Apple Frameworks
conan_find_apple_frameworks(CONAN_FRAMEWORKS_FOUND_URDFDOMCPP "${CONAN_FRAMEWORKS_URDFDOMCPP}" "_URDFDOMCPP" "")
# Append to aggregated values variable
set(CONAN_LIBS_URDFDOMCPP ${CONAN_PKG_LIBS_URDFDOMCPP} ${CONAN_SYSTEM_LIBS_URDFDOMCPP} ${CONAN_FRAMEWORKS_FOUND_URDFDOMCPP})


#################
###  EIGEN3
#################
set(CONAN_EIGEN3_ROOT "/home/hamza/.conan/data/eigen/3.3.7/conan/stable/package/5ab84d6acfe1f23c4fae0ab88f26e3a396351ac9")
set(CONAN_INCLUDE_DIRS_EIGEN3 "/home/hamza/.conan/data/eigen/3.3.7/conan/stable/package/5ab84d6acfe1f23c4fae0ab88f26e3a396351ac9/include/eigen3")
set(CONAN_LIB_DIRS_EIGEN3 )
set(CONAN_BIN_DIRS_EIGEN3 )
set(CONAN_RES_DIRS_EIGEN3 )
set(CONAN_SRC_DIRS_EIGEN3 )
set(CONAN_BUILD_DIRS_EIGEN3 "/home/hamza/.conan/data/eigen/3.3.7/conan/stable/package/5ab84d6acfe1f23c4fae0ab88f26e3a396351ac9/")
set(CONAN_FRAMEWORK_DIRS_EIGEN3 )
set(CONAN_LIBS_EIGEN3 )
set(CONAN_PKG_LIBS_EIGEN3 )
set(CONAN_SYSTEM_LIBS_EIGEN3 )
set(CONAN_FRAMEWORKS_EIGEN3 )
set(CONAN_FRAMEWORKS_FOUND_EIGEN3 "")  # Will be filled later
set(CONAN_DEFINES_EIGEN3 )
set(CONAN_BUILD_MODULES_PATHS_EIGEN3 )
# COMPILE_DEFINITIONS are equal to CONAN_DEFINES without -D, for targets
set(CONAN_COMPILE_DEFINITIONS_EIGEN3 )

set(CONAN_C_FLAGS_EIGEN3 "")
set(CONAN_CXX_FLAGS_EIGEN3 "")
set(CONAN_SHARED_LINKER_FLAGS_EIGEN3 "")
set(CONAN_EXE_LINKER_FLAGS_EIGEN3 "")

# For modern cmake targets we use the list variables (separated with ;)
set(CONAN_C_FLAGS_EIGEN3_LIST "")
set(CONAN_CXX_FLAGS_EIGEN3_LIST "")
set(CONAN_SHARED_LINKER_FLAGS_EIGEN3_LIST "")
set(CONAN_EXE_LINKER_FLAGS_EIGEN3_LIST "")

# Apple Frameworks
conan_find_apple_frameworks(CONAN_FRAMEWORKS_FOUND_EIGEN3 "${CONAN_FRAMEWORKS_EIGEN3}" "_EIGEN3" "")
# Append to aggregated values variable
set(CONAN_LIBS_EIGEN3 ${CONAN_PKG_LIBS_EIGEN3} ${CONAN_SYSTEM_LIBS_EIGEN3} ${CONAN_FRAMEWORKS_FOUND_EIGEN3})


#################
###  FMT
#################
set(CONAN_FMT_ROOT "/home/hamza/.conan/data/fmt/7.0.1/_/_/package/66c5327ebdcecae0a01a863939964495fa019a06")
set(CONAN_INCLUDE_DIRS_FMT "/home/hamza/.conan/data/fmt/7.0.1/_/_/package/66c5327ebdcecae0a01a863939964495fa019a06/include")
set(CONAN_LIB_DIRS_FMT "/home/hamza/.conan/data/fmt/7.0.1/_/_/package/66c5327ebdcecae0a01a863939964495fa019a06/lib")
set(CONAN_BIN_DIRS_FMT )
set(CONAN_RES_DIRS_FMT )
set(CONAN_SRC_DIRS_FMT )
set(CONAN_BUILD_DIRS_FMT "/home/hamza/.conan/data/fmt/7.0.1/_/_/package/66c5327ebdcecae0a01a863939964495fa019a06/")
set(CONAN_FRAMEWORK_DIRS_FMT )
set(CONAN_LIBS_FMT fmt)
set(CONAN_PKG_LIBS_FMT fmt)
set(CONAN_SYSTEM_LIBS_FMT )
set(CONAN_FRAMEWORKS_FMT )
set(CONAN_FRAMEWORKS_FOUND_FMT "")  # Will be filled later
set(CONAN_DEFINES_FMT )
set(CONAN_BUILD_MODULES_PATHS_FMT )
# COMPILE_DEFINITIONS are equal to CONAN_DEFINES without -D, for targets
set(CONAN_COMPILE_DEFINITIONS_FMT )

set(CONAN_C_FLAGS_FMT "")
set(CONAN_CXX_FLAGS_FMT "")
set(CONAN_SHARED_LINKER_FLAGS_FMT "")
set(CONAN_EXE_LINKER_FLAGS_FMT "")

# For modern cmake targets we use the list variables (separated with ;)
set(CONAN_C_FLAGS_FMT_LIST "")
set(CONAN_CXX_FLAGS_FMT_LIST "")
set(CONAN_SHARED_LINKER_FLAGS_FMT_LIST "")
set(CONAN_EXE_LINKER_FLAGS_FMT_LIST "")

# Apple Frameworks
conan_find_apple_frameworks(CONAN_FRAMEWORKS_FOUND_FMT "${CONAN_FRAMEWORKS_FMT}" "_FMT" "")
# Append to aggregated values variable
set(CONAN_LIBS_FMT ${CONAN_PKG_LIBS_FMT} ${CONAN_SYSTEM_LIBS_FMT} ${CONAN_FRAMEWORKS_FOUND_FMT})


#################
###  OSQP
#################
set(CONAN_OSQP_ROOT "/home/hamza/.conan/data/osqp/0.6.0/bnavarro/stable/package/9bfdcfa2bb925892ecf42e2a018a3f3529826676")
set(CONAN_INCLUDE_DIRS_OSQP "/home/hamza/.conan/data/osqp/0.6.0/bnavarro/stable/package/9bfdcfa2bb925892ecf42e2a018a3f3529826676/include/osqp"
			"/home/hamza/.conan/data/osqp/0.6.0/bnavarro/stable/package/9bfdcfa2bb925892ecf42e2a018a3f3529826676/include/qdldl")
set(CONAN_LIB_DIRS_OSQP "/home/hamza/.conan/data/osqp/0.6.0/bnavarro/stable/package/9bfdcfa2bb925892ecf42e2a018a3f3529826676/lib")
set(CONAN_BIN_DIRS_OSQP )
set(CONAN_RES_DIRS_OSQP )
set(CONAN_SRC_DIRS_OSQP )
set(CONAN_BUILD_DIRS_OSQP "/home/hamza/.conan/data/osqp/0.6.0/bnavarro/stable/package/9bfdcfa2bb925892ecf42e2a018a3f3529826676/")
set(CONAN_FRAMEWORK_DIRS_OSQP )
set(CONAN_LIBS_OSQP osqp qdldl)
set(CONAN_PKG_LIBS_OSQP osqp qdldl)
set(CONAN_SYSTEM_LIBS_OSQP )
set(CONAN_FRAMEWORKS_OSQP )
set(CONAN_FRAMEWORKS_FOUND_OSQP "")  # Will be filled later
set(CONAN_DEFINES_OSQP )
set(CONAN_BUILD_MODULES_PATHS_OSQP )
# COMPILE_DEFINITIONS are equal to CONAN_DEFINES without -D, for targets
set(CONAN_COMPILE_DEFINITIONS_OSQP )

set(CONAN_C_FLAGS_OSQP "")
set(CONAN_CXX_FLAGS_OSQP "")
set(CONAN_SHARED_LINKER_FLAGS_OSQP "")
set(CONAN_EXE_LINKER_FLAGS_OSQP "")

# For modern cmake targets we use the list variables (separated with ;)
set(CONAN_C_FLAGS_OSQP_LIST "")
set(CONAN_CXX_FLAGS_OSQP_LIST "")
set(CONAN_SHARED_LINKER_FLAGS_OSQP_LIST "")
set(CONAN_EXE_LINKER_FLAGS_OSQP_LIST "")

# Apple Frameworks
conan_find_apple_frameworks(CONAN_FRAMEWORKS_FOUND_OSQP "${CONAN_FRAMEWORKS_OSQP}" "_OSQP" "")
# Append to aggregated values variable
set(CONAN_LIBS_OSQP ${CONAN_PKG_LIBS_OSQP} ${CONAN_SYSTEM_LIBS_OSQP} ${CONAN_FRAMEWORKS_FOUND_OSQP})


#################
###  ECOS
#################
set(CONAN_ECOS_ROOT "/home/hamza/.conan/data/ecos/2.0.7/bnavarro/stable/package/9bfdcfa2bb925892ecf42e2a018a3f3529826676")
set(CONAN_INCLUDE_DIRS_ECOS "/home/hamza/.conan/data/ecos/2.0.7/bnavarro/stable/package/9bfdcfa2bb925892ecf42e2a018a3f3529826676/include/ecos"
			"/home/hamza/.conan/data/ecos/2.0.7/bnavarro/stable/package/9bfdcfa2bb925892ecf42e2a018a3f3529826676/include/SuiteSparse_config")
set(CONAN_LIB_DIRS_ECOS "/home/hamza/.conan/data/ecos/2.0.7/bnavarro/stable/package/9bfdcfa2bb925892ecf42e2a018a3f3529826676/lib")
set(CONAN_BIN_DIRS_ECOS )
set(CONAN_RES_DIRS_ECOS )
set(CONAN_SRC_DIRS_ECOS )
set(CONAN_BUILD_DIRS_ECOS "/home/hamza/.conan/data/ecos/2.0.7/bnavarro/stable/package/9bfdcfa2bb925892ecf42e2a018a3f3529826676/")
set(CONAN_FRAMEWORK_DIRS_ECOS )
set(CONAN_LIBS_ECOS ecos)
set(CONAN_PKG_LIBS_ECOS ecos)
set(CONAN_SYSTEM_LIBS_ECOS )
set(CONAN_FRAMEWORKS_ECOS )
set(CONAN_FRAMEWORKS_FOUND_ECOS "")  # Will be filled later
set(CONAN_DEFINES_ECOS "-DCTRLC=1"
			"-DLDL_LONG"
			"-DDLONG")
set(CONAN_BUILD_MODULES_PATHS_ECOS )
# COMPILE_DEFINITIONS are equal to CONAN_DEFINES without -D, for targets
set(CONAN_COMPILE_DEFINITIONS_ECOS "CTRLC=1"
			"LDL_LONG"
			"DLONG")

set(CONAN_C_FLAGS_ECOS "")
set(CONAN_CXX_FLAGS_ECOS "")
set(CONAN_SHARED_LINKER_FLAGS_ECOS "")
set(CONAN_EXE_LINKER_FLAGS_ECOS "")

# For modern cmake targets we use the list variables (separated with ;)
set(CONAN_C_FLAGS_ECOS_LIST "")
set(CONAN_CXX_FLAGS_ECOS_LIST "")
set(CONAN_SHARED_LINKER_FLAGS_ECOS_LIST "")
set(CONAN_EXE_LINKER_FLAGS_ECOS_LIST "")

# Apple Frameworks
conan_find_apple_frameworks(CONAN_FRAMEWORKS_FOUND_ECOS "${CONAN_FRAMEWORKS_ECOS}" "_ECOS" "")
# Append to aggregated values variable
set(CONAN_LIBS_ECOS ${CONAN_PKG_LIBS_ECOS} ${CONAN_SYSTEM_LIBS_ECOS} ${CONAN_FRAMEWORKS_FOUND_ECOS})


#################
###  TINYXML2
#################
set(CONAN_TINYXML2_ROOT "/home/hamza/.conan/data/tinyxml2/8.0.0/_/_/package/66c5327ebdcecae0a01a863939964495fa019a06")
set(CONAN_INCLUDE_DIRS_TINYXML2 "/home/hamza/.conan/data/tinyxml2/8.0.0/_/_/package/66c5327ebdcecae0a01a863939964495fa019a06/include")
set(CONAN_LIB_DIRS_TINYXML2 "/home/hamza/.conan/data/tinyxml2/8.0.0/_/_/package/66c5327ebdcecae0a01a863939964495fa019a06/lib")
set(CONAN_BIN_DIRS_TINYXML2 )
set(CONAN_RES_DIRS_TINYXML2 )
set(CONAN_SRC_DIRS_TINYXML2 )
set(CONAN_BUILD_DIRS_TINYXML2 "/home/hamza/.conan/data/tinyxml2/8.0.0/_/_/package/66c5327ebdcecae0a01a863939964495fa019a06/")
set(CONAN_FRAMEWORK_DIRS_TINYXML2 )
set(CONAN_LIBS_TINYXML2 tinyxml2)
set(CONAN_PKG_LIBS_TINYXML2 tinyxml2)
set(CONAN_SYSTEM_LIBS_TINYXML2 )
set(CONAN_FRAMEWORKS_TINYXML2 )
set(CONAN_FRAMEWORKS_FOUND_TINYXML2 "")  # Will be filled later
set(CONAN_DEFINES_TINYXML2 )
set(CONAN_BUILD_MODULES_PATHS_TINYXML2 )
# COMPILE_DEFINITIONS are equal to CONAN_DEFINES without -D, for targets
set(CONAN_COMPILE_DEFINITIONS_TINYXML2 )

set(CONAN_C_FLAGS_TINYXML2 "")
set(CONAN_CXX_FLAGS_TINYXML2 "")
set(CONAN_SHARED_LINKER_FLAGS_TINYXML2 "")
set(CONAN_EXE_LINKER_FLAGS_TINYXML2 "")

# For modern cmake targets we use the list variables (separated with ;)
set(CONAN_C_FLAGS_TINYXML2_LIST "")
set(CONAN_CXX_FLAGS_TINYXML2_LIST "")
set(CONAN_SHARED_LINKER_FLAGS_TINYXML2_LIST "")
set(CONAN_EXE_LINKER_FLAGS_TINYXML2_LIST "")

# Apple Frameworks
conan_find_apple_frameworks(CONAN_FRAMEWORKS_FOUND_TINYXML2 "${CONAN_FRAMEWORKS_TINYXML2}" "_TINYXML2" "")
# Append to aggregated values variable
set(CONAN_LIBS_TINYXML2 ${CONAN_PKG_LIBS_TINYXML2} ${CONAN_SYSTEM_LIBS_TINYXML2} ${CONAN_FRAMEWORKS_FOUND_TINYXML2})


### Definition of global aggregated variables ###

set(CONAN_PACKAGE_NAME None)
set(CONAN_PACKAGE_VERSION None)

set(CONAN_SETTINGS_ARCH "x86_64")
set(CONAN_SETTINGS_ARCH_BUILD "x86_64")
set(CONAN_SETTINGS_BUILD_TYPE "Release")
set(CONAN_SETTINGS_COMPILER "gcc")
set(CONAN_SETTINGS_COMPILER_LIBCXX "libstdc++11")
set(CONAN_SETTINGS_COMPILER_VERSION "7")
set(CONAN_SETTINGS_OS "Linux")
set(CONAN_SETTINGS_OS_BUILD "Linux")

set(CONAN_DEPENDENCIES robot-trajectory robot-control robot-model eigen-extensions epigraph urdfdomcpp eigen fmt osqp ecos tinyxml2)
# Storing original command line args (CMake helper) flags
set(CONAN_CMD_CXX_FLAGS ${CONAN_CXX_FLAGS})

set(CONAN_CMD_SHARED_LINKER_FLAGS ${CONAN_SHARED_LINKER_FLAGS})
set(CONAN_CMD_C_FLAGS ${CONAN_C_FLAGS})
# Defining accumulated conan variables for all deps

set(CONAN_INCLUDE_DIRS "/home/hamza/.conan/data/robot-trajectory/1.0/bnavarro/testing/package/d6b96dcf702dae5b449f2dc4f8e363f049487c4b/include"
			"/home/hamza/.conan/data/robot-control/1.0/bnavarro/testing/package/22e5a59e91a5280d237198bd58f1bba2144479b2/include"
			"/home/hamza/.conan/data/robot-model/1.0/bnavarro/testing/package/7ebec6d21ff02bfaccef00531a647309ae998255/include"
			"/home/hamza/.conan/data/eigen-extensions/0.13.2/bnavarro/testing/package/5ab84d6acfe1f23c4fae0ab88f26e3a396351ac9/include"
			"/home/hamza/.conan/data/epigraph/0.4.0/bnavarro/stable/package/1d7ac1c9c1daee8278f56b35a8bf95277d9fad5c/include"
			"/home/hamza/.conan/data/urdfdomcpp/1.0/bnavarro/stable/package/c62e1023a11546ac53345798073dd54f92db8e1c/include"
			"/home/hamza/.conan/data/eigen/3.3.7/conan/stable/package/5ab84d6acfe1f23c4fae0ab88f26e3a396351ac9/include/eigen3"
			"/home/hamza/.conan/data/fmt/7.0.1/_/_/package/66c5327ebdcecae0a01a863939964495fa019a06/include"
			"/home/hamza/.conan/data/osqp/0.6.0/bnavarro/stable/package/9bfdcfa2bb925892ecf42e2a018a3f3529826676/include/osqp"
			"/home/hamza/.conan/data/osqp/0.6.0/bnavarro/stable/package/9bfdcfa2bb925892ecf42e2a018a3f3529826676/include/qdldl"
			"/home/hamza/.conan/data/ecos/2.0.7/bnavarro/stable/package/9bfdcfa2bb925892ecf42e2a018a3f3529826676/include/ecos"
			"/home/hamza/.conan/data/ecos/2.0.7/bnavarro/stable/package/9bfdcfa2bb925892ecf42e2a018a3f3529826676/include/SuiteSparse_config"
			"/home/hamza/.conan/data/tinyxml2/8.0.0/_/_/package/66c5327ebdcecae0a01a863939964495fa019a06/include" ${CONAN_INCLUDE_DIRS})
set(CONAN_LIB_DIRS "/home/hamza/.conan/data/robot-trajectory/1.0/bnavarro/testing/package/d6b96dcf702dae5b449f2dc4f8e363f049487c4b/lib"
			"/home/hamza/.conan/data/robot-control/1.0/bnavarro/testing/package/22e5a59e91a5280d237198bd58f1bba2144479b2/lib"
			"/home/hamza/.conan/data/robot-model/1.0/bnavarro/testing/package/7ebec6d21ff02bfaccef00531a647309ae998255/lib"
			"/home/hamza/.conan/data/epigraph/0.4.0/bnavarro/stable/package/1d7ac1c9c1daee8278f56b35a8bf95277d9fad5c/lib"
			"/home/hamza/.conan/data/urdfdomcpp/1.0/bnavarro/stable/package/c62e1023a11546ac53345798073dd54f92db8e1c/lib"
			"/home/hamza/.conan/data/fmt/7.0.1/_/_/package/66c5327ebdcecae0a01a863939964495fa019a06/lib"
			"/home/hamza/.conan/data/osqp/0.6.0/bnavarro/stable/package/9bfdcfa2bb925892ecf42e2a018a3f3529826676/lib"
			"/home/hamza/.conan/data/ecos/2.0.7/bnavarro/stable/package/9bfdcfa2bb925892ecf42e2a018a3f3529826676/lib"
			"/home/hamza/.conan/data/tinyxml2/8.0.0/_/_/package/66c5327ebdcecae0a01a863939964495fa019a06/lib" ${CONAN_LIB_DIRS})
set(CONAN_BIN_DIRS "/home/hamza/.conan/data/robot-trajectory/1.0/bnavarro/testing/package/d6b96dcf702dae5b449f2dc4f8e363f049487c4b/bin"
			"/home/hamza/.conan/data/robot-control/1.0/bnavarro/testing/package/22e5a59e91a5280d237198bd58f1bba2144479b2/bin"
			"/home/hamza/.conan/data/robot-model/1.0/bnavarro/testing/package/7ebec6d21ff02bfaccef00531a647309ae998255/bin"
			"/home/hamza/.conan/data/urdfdomcpp/1.0/bnavarro/stable/package/c62e1023a11546ac53345798073dd54f92db8e1c/bin" ${CONAN_BIN_DIRS})
set(CONAN_RES_DIRS  ${CONAN_RES_DIRS})
set(CONAN_FRAMEWORK_DIRS  ${CONAN_FRAMEWORK_DIRS})
set(CONAN_LIBS robot_trajectory robot_control robot_model epigraph urdfdomcpp fmt osqp qdldl ecos tinyxml2 ${CONAN_LIBS})
set(CONAN_PKG_LIBS robot_trajectory robot_control robot_model epigraph urdfdomcpp fmt osqp qdldl ecos tinyxml2 ${CONAN_PKG_LIBS})
set(CONAN_SYSTEM_LIBS  ${CONAN_SYSTEM_LIBS})
set(CONAN_FRAMEWORKS  ${CONAN_FRAMEWORKS})
set(CONAN_FRAMEWORKS_FOUND "")  # Will be filled later
set(CONAN_DEFINES "-DCTRLC=1"
			"-DLDL_LONG"
			"-DDLONG"
			"-DENABLE_OSQP"
			"-DENABLE_ECOS"
			"-DEIGEN_DENSEBASE_PLUGIN=<Eigen/dense_base_extensions.h>"
			"-DEIGEN_QUATERNIONBASE_PLUGIN=<Eigen/quaternion_base_extensions.h>" ${CONAN_DEFINES})
set(CONAN_BUILD_MODULES_PATHS  ${CONAN_BUILD_MODULES_PATHS})
set(CONAN_CMAKE_MODULE_PATH "/home/hamza/.conan/data/robot-trajectory/1.0/bnavarro/testing/package/d6b96dcf702dae5b449f2dc4f8e363f049487c4b/"
			"/home/hamza/.conan/data/robot-control/1.0/bnavarro/testing/package/22e5a59e91a5280d237198bd58f1bba2144479b2/"
			"/home/hamza/.conan/data/robot-model/1.0/bnavarro/testing/package/7ebec6d21ff02bfaccef00531a647309ae998255/"
			"/home/hamza/.conan/data/eigen-extensions/0.13.2/bnavarro/testing/package/5ab84d6acfe1f23c4fae0ab88f26e3a396351ac9/"
			"/home/hamza/.conan/data/epigraph/0.4.0/bnavarro/stable/package/1d7ac1c9c1daee8278f56b35a8bf95277d9fad5c/"
			"/home/hamza/.conan/data/urdfdomcpp/1.0/bnavarro/stable/package/c62e1023a11546ac53345798073dd54f92db8e1c/"
			"/home/hamza/.conan/data/eigen/3.3.7/conan/stable/package/5ab84d6acfe1f23c4fae0ab88f26e3a396351ac9/"
			"/home/hamza/.conan/data/fmt/7.0.1/_/_/package/66c5327ebdcecae0a01a863939964495fa019a06/"
			"/home/hamza/.conan/data/osqp/0.6.0/bnavarro/stable/package/9bfdcfa2bb925892ecf42e2a018a3f3529826676/"
			"/home/hamza/.conan/data/ecos/2.0.7/bnavarro/stable/package/9bfdcfa2bb925892ecf42e2a018a3f3529826676/"
			"/home/hamza/.conan/data/tinyxml2/8.0.0/_/_/package/66c5327ebdcecae0a01a863939964495fa019a06/" ${CONAN_CMAKE_MODULE_PATH})

set(CONAN_CXX_FLAGS " ${CONAN_CXX_FLAGS}")
set(CONAN_SHARED_LINKER_FLAGS " ${CONAN_SHARED_LINKER_FLAGS}")
set(CONAN_EXE_LINKER_FLAGS " ${CONAN_EXE_LINKER_FLAGS}")
set(CONAN_C_FLAGS " ${CONAN_C_FLAGS}")

# Apple Frameworks
conan_find_apple_frameworks(CONAN_FRAMEWORKS_FOUND "${CONAN_FRAMEWORKS}" "" "")
# Append to aggregated values variable: Use CONAN_LIBS instead of CONAN_PKG_LIBS to include user appended vars
set(CONAN_LIBS ${CONAN_LIBS} ${CONAN_SYSTEM_LIBS} ${CONAN_FRAMEWORKS_FOUND})


###  Definition of macros and functions ###

macro(conan_define_targets)
    if(${CMAKE_VERSION} VERSION_LESS "3.1.2")
        message(FATAL_ERROR "TARGETS not supported by your CMake version!")
    endif()  # CMAKE > 3.x
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${CONAN_CMD_CXX_FLAGS}")
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${CONAN_CMD_C_FLAGS}")
    set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} ${CONAN_CMD_SHARED_LINKER_FLAGS}")


    set(_CONAN_PKG_LIBS_ROBOT-TRAJECTORY_DEPENDENCIES "${CONAN_SYSTEM_LIBS_ROBOT-TRAJECTORY} ${CONAN_FRAMEWORKS_FOUND_ROBOT-TRAJECTORY} CONAN_PKG::fmt CONAN_PKG::eigen-extensions")
    string(REPLACE " " ";" _CONAN_PKG_LIBS_ROBOT-TRAJECTORY_DEPENDENCIES "${_CONAN_PKG_LIBS_ROBOT-TRAJECTORY_DEPENDENCIES}")
    conan_package_library_targets("${CONAN_PKG_LIBS_ROBOT-TRAJECTORY}" "${CONAN_LIB_DIRS_ROBOT-TRAJECTORY}"
                                  CONAN_PACKAGE_TARGETS_ROBOT-TRAJECTORY "${_CONAN_PKG_LIBS_ROBOT-TRAJECTORY_DEPENDENCIES}"
                                  "" robot-trajectory)
    set(_CONAN_PKG_LIBS_ROBOT-TRAJECTORY_DEPENDENCIES_DEBUG "${CONAN_SYSTEM_LIBS_ROBOT-TRAJECTORY_DEBUG} ${CONAN_FRAMEWORKS_FOUND_ROBOT-TRAJECTORY_DEBUG} CONAN_PKG::fmt CONAN_PKG::eigen-extensions")
    string(REPLACE " " ";" _CONAN_PKG_LIBS_ROBOT-TRAJECTORY_DEPENDENCIES_DEBUG "${_CONAN_PKG_LIBS_ROBOT-TRAJECTORY_DEPENDENCIES_DEBUG}")
    conan_package_library_targets("${CONAN_PKG_LIBS_ROBOT-TRAJECTORY_DEBUG}" "${CONAN_LIB_DIRS_ROBOT-TRAJECTORY_DEBUG}"
                                  CONAN_PACKAGE_TARGETS_ROBOT-TRAJECTORY_DEBUG "${_CONAN_PKG_LIBS_ROBOT-TRAJECTORY_DEPENDENCIES_DEBUG}"
                                  "debug" robot-trajectory)
    set(_CONAN_PKG_LIBS_ROBOT-TRAJECTORY_DEPENDENCIES_RELEASE "${CONAN_SYSTEM_LIBS_ROBOT-TRAJECTORY_RELEASE} ${CONAN_FRAMEWORKS_FOUND_ROBOT-TRAJECTORY_RELEASE} CONAN_PKG::fmt CONAN_PKG::eigen-extensions")
    string(REPLACE " " ";" _CONAN_PKG_LIBS_ROBOT-TRAJECTORY_DEPENDENCIES_RELEASE "${_CONAN_PKG_LIBS_ROBOT-TRAJECTORY_DEPENDENCIES_RELEASE}")
    conan_package_library_targets("${CONAN_PKG_LIBS_ROBOT-TRAJECTORY_RELEASE}" "${CONAN_LIB_DIRS_ROBOT-TRAJECTORY_RELEASE}"
                                  CONAN_PACKAGE_TARGETS_ROBOT-TRAJECTORY_RELEASE "${_CONAN_PKG_LIBS_ROBOT-TRAJECTORY_DEPENDENCIES_RELEASE}"
                                  "release" robot-trajectory)
    set(_CONAN_PKG_LIBS_ROBOT-TRAJECTORY_DEPENDENCIES_RELWITHDEBINFO "${CONAN_SYSTEM_LIBS_ROBOT-TRAJECTORY_RELWITHDEBINFO} ${CONAN_FRAMEWORKS_FOUND_ROBOT-TRAJECTORY_RELWITHDEBINFO} CONAN_PKG::fmt CONAN_PKG::eigen-extensions")
    string(REPLACE " " ";" _CONAN_PKG_LIBS_ROBOT-TRAJECTORY_DEPENDENCIES_RELWITHDEBINFO "${_CONAN_PKG_LIBS_ROBOT-TRAJECTORY_DEPENDENCIES_RELWITHDEBINFO}")
    conan_package_library_targets("${CONAN_PKG_LIBS_ROBOT-TRAJECTORY_RELWITHDEBINFO}" "${CONAN_LIB_DIRS_ROBOT-TRAJECTORY_RELWITHDEBINFO}"
                                  CONAN_PACKAGE_TARGETS_ROBOT-TRAJECTORY_RELWITHDEBINFO "${_CONAN_PKG_LIBS_ROBOT-TRAJECTORY_DEPENDENCIES_RELWITHDEBINFO}"
                                  "relwithdebinfo" robot-trajectory)
    set(_CONAN_PKG_LIBS_ROBOT-TRAJECTORY_DEPENDENCIES_MINSIZEREL "${CONAN_SYSTEM_LIBS_ROBOT-TRAJECTORY_MINSIZEREL} ${CONAN_FRAMEWORKS_FOUND_ROBOT-TRAJECTORY_MINSIZEREL} CONAN_PKG::fmt CONAN_PKG::eigen-extensions")
    string(REPLACE " " ";" _CONAN_PKG_LIBS_ROBOT-TRAJECTORY_DEPENDENCIES_MINSIZEREL "${_CONAN_PKG_LIBS_ROBOT-TRAJECTORY_DEPENDENCIES_MINSIZEREL}")
    conan_package_library_targets("${CONAN_PKG_LIBS_ROBOT-TRAJECTORY_MINSIZEREL}" "${CONAN_LIB_DIRS_ROBOT-TRAJECTORY_MINSIZEREL}"
                                  CONAN_PACKAGE_TARGETS_ROBOT-TRAJECTORY_MINSIZEREL "${_CONAN_PKG_LIBS_ROBOT-TRAJECTORY_DEPENDENCIES_MINSIZEREL}"
                                  "minsizerel" robot-trajectory)

    add_library(CONAN_PKG::robot-trajectory INTERFACE IMPORTED)

    # Property INTERFACE_LINK_FLAGS do not work, necessary to add to INTERFACE_LINK_LIBRARIES
    set_property(TARGET CONAN_PKG::robot-trajectory PROPERTY INTERFACE_LINK_LIBRARIES ${CONAN_PACKAGE_TARGETS_ROBOT-TRAJECTORY} ${_CONAN_PKG_LIBS_ROBOT-TRAJECTORY_DEPENDENCIES}
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_ROBOT-TRAJECTORY_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_ROBOT-TRAJECTORY_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:${CONAN_EXE_LINKER_FLAGS_ROBOT-TRAJECTORY_LIST}>

                                                                 $<$<CONFIG:Release>:${CONAN_PACKAGE_TARGETS_ROBOT-TRAJECTORY_RELEASE} ${_CONAN_PKG_LIBS_ROBOT-TRAJECTORY_DEPENDENCIES_RELEASE}
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_ROBOT-TRAJECTORY_RELEASE_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_ROBOT-TRAJECTORY_RELEASE_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:${CONAN_EXE_LINKER_FLAGS_ROBOT-TRAJECTORY_RELEASE_LIST}>>

                                                                 $<$<CONFIG:RelWithDebInfo>:${CONAN_PACKAGE_TARGETS_ROBOT-TRAJECTORY_RELWITHDEBINFO} ${_CONAN_PKG_LIBS_ROBOT-TRAJECTORY_DEPENDENCIES_RELWITHDEBINFO}
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_ROBOT-TRAJECTORY_RELWITHDEBINFO_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_ROBOT-TRAJECTORY_RELWITHDEBINFO_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:${CONAN_EXE_LINKER_FLAGS_ROBOT-TRAJECTORY_RELWITHDEBINFO_LIST}>>

                                                                 $<$<CONFIG:MinSizeRel>:${CONAN_PACKAGE_TARGETS_ROBOT-TRAJECTORY_MINSIZEREL} ${_CONAN_PKG_LIBS_ROBOT-TRAJECTORY_DEPENDENCIES_MINSIZEREL}
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_ROBOT-TRAJECTORY_MINSIZEREL_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_ROBOT-TRAJECTORY_MINSIZEREL_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:${CONAN_EXE_LINKER_FLAGS_ROBOT-TRAJECTORY_MINSIZEREL_LIST}>>

                                                                 $<$<CONFIG:Debug>:${CONAN_PACKAGE_TARGETS_ROBOT-TRAJECTORY_DEBUG} ${_CONAN_PKG_LIBS_ROBOT-TRAJECTORY_DEPENDENCIES_DEBUG}
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_ROBOT-TRAJECTORY_DEBUG_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_ROBOT-TRAJECTORY_DEBUG_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:${CONAN_EXE_LINKER_FLAGS_ROBOT-TRAJECTORY_DEBUG_LIST}>>)
    set_property(TARGET CONAN_PKG::robot-trajectory PROPERTY INTERFACE_INCLUDE_DIRECTORIES ${CONAN_INCLUDE_DIRS_ROBOT-TRAJECTORY}
                                                                      $<$<CONFIG:Release>:${CONAN_INCLUDE_DIRS_ROBOT-TRAJECTORY_RELEASE}>
                                                                      $<$<CONFIG:RelWithDebInfo>:${CONAN_INCLUDE_DIRS_ROBOT-TRAJECTORY_RELWITHDEBINFO}>
                                                                      $<$<CONFIG:MinSizeRel>:${CONAN_INCLUDE_DIRS_ROBOT-TRAJECTORY_MINSIZEREL}>
                                                                      $<$<CONFIG:Debug>:${CONAN_INCLUDE_DIRS_ROBOT-TRAJECTORY_DEBUG}>)
    set_property(TARGET CONAN_PKG::robot-trajectory PROPERTY INTERFACE_COMPILE_DEFINITIONS ${CONAN_COMPILE_DEFINITIONS_ROBOT-TRAJECTORY}
                                                                      $<$<CONFIG:Release>:${CONAN_COMPILE_DEFINITIONS_ROBOT-TRAJECTORY_RELEASE}>
                                                                      $<$<CONFIG:RelWithDebInfo>:${CONAN_COMPILE_DEFINITIONS_ROBOT-TRAJECTORY_RELWITHDEBINFO}>
                                                                      $<$<CONFIG:MinSizeRel>:${CONAN_COMPILE_DEFINITIONS_ROBOT-TRAJECTORY_MINSIZEREL}>
                                                                      $<$<CONFIG:Debug>:${CONAN_COMPILE_DEFINITIONS_ROBOT-TRAJECTORY_DEBUG}>)
    set_property(TARGET CONAN_PKG::robot-trajectory PROPERTY INTERFACE_COMPILE_OPTIONS ${CONAN_C_FLAGS_ROBOT-TRAJECTORY_LIST} ${CONAN_CXX_FLAGS_ROBOT-TRAJECTORY_LIST}
                                                                  $<$<CONFIG:Release>:${CONAN_C_FLAGS_ROBOT-TRAJECTORY_RELEASE_LIST} ${CONAN_CXX_FLAGS_ROBOT-TRAJECTORY_RELEASE_LIST}>
                                                                  $<$<CONFIG:RelWithDebInfo>:${CONAN_C_FLAGS_ROBOT-TRAJECTORY_RELWITHDEBINFO_LIST} ${CONAN_CXX_FLAGS_ROBOT-TRAJECTORY_RELWITHDEBINFO_LIST}>
                                                                  $<$<CONFIG:MinSizeRel>:${CONAN_C_FLAGS_ROBOT-TRAJECTORY_MINSIZEREL_LIST} ${CONAN_CXX_FLAGS_ROBOT-TRAJECTORY_MINSIZEREL_LIST}>
                                                                  $<$<CONFIG:Debug>:${CONAN_C_FLAGS_ROBOT-TRAJECTORY_DEBUG_LIST}  ${CONAN_CXX_FLAGS_ROBOT-TRAJECTORY_DEBUG_LIST}>)


    set(_CONAN_PKG_LIBS_ROBOT-CONTROL_DEPENDENCIES "${CONAN_SYSTEM_LIBS_ROBOT-CONTROL} ${CONAN_FRAMEWORKS_FOUND_ROBOT-CONTROL} CONAN_PKG::fmt CONAN_PKG::robot-model CONAN_PKG::eigen-extensions CONAN_PKG::epigraph")
    string(REPLACE " " ";" _CONAN_PKG_LIBS_ROBOT-CONTROL_DEPENDENCIES "${_CONAN_PKG_LIBS_ROBOT-CONTROL_DEPENDENCIES}")
    conan_package_library_targets("${CONAN_PKG_LIBS_ROBOT-CONTROL}" "${CONAN_LIB_DIRS_ROBOT-CONTROL}"
                                  CONAN_PACKAGE_TARGETS_ROBOT-CONTROL "${_CONAN_PKG_LIBS_ROBOT-CONTROL_DEPENDENCIES}"
                                  "" robot-control)
    set(_CONAN_PKG_LIBS_ROBOT-CONTROL_DEPENDENCIES_DEBUG "${CONAN_SYSTEM_LIBS_ROBOT-CONTROL_DEBUG} ${CONAN_FRAMEWORKS_FOUND_ROBOT-CONTROL_DEBUG} CONAN_PKG::fmt CONAN_PKG::robot-model CONAN_PKG::eigen-extensions CONAN_PKG::epigraph")
    string(REPLACE " " ";" _CONAN_PKG_LIBS_ROBOT-CONTROL_DEPENDENCIES_DEBUG "${_CONAN_PKG_LIBS_ROBOT-CONTROL_DEPENDENCIES_DEBUG}")
    conan_package_library_targets("${CONAN_PKG_LIBS_ROBOT-CONTROL_DEBUG}" "${CONAN_LIB_DIRS_ROBOT-CONTROL_DEBUG}"
                                  CONAN_PACKAGE_TARGETS_ROBOT-CONTROL_DEBUG "${_CONAN_PKG_LIBS_ROBOT-CONTROL_DEPENDENCIES_DEBUG}"
                                  "debug" robot-control)
    set(_CONAN_PKG_LIBS_ROBOT-CONTROL_DEPENDENCIES_RELEASE "${CONAN_SYSTEM_LIBS_ROBOT-CONTROL_RELEASE} ${CONAN_FRAMEWORKS_FOUND_ROBOT-CONTROL_RELEASE} CONAN_PKG::fmt CONAN_PKG::robot-model CONAN_PKG::eigen-extensions CONAN_PKG::epigraph")
    string(REPLACE " " ";" _CONAN_PKG_LIBS_ROBOT-CONTROL_DEPENDENCIES_RELEASE "${_CONAN_PKG_LIBS_ROBOT-CONTROL_DEPENDENCIES_RELEASE}")
    conan_package_library_targets("${CONAN_PKG_LIBS_ROBOT-CONTROL_RELEASE}" "${CONAN_LIB_DIRS_ROBOT-CONTROL_RELEASE}"
                                  CONAN_PACKAGE_TARGETS_ROBOT-CONTROL_RELEASE "${_CONAN_PKG_LIBS_ROBOT-CONTROL_DEPENDENCIES_RELEASE}"
                                  "release" robot-control)
    set(_CONAN_PKG_LIBS_ROBOT-CONTROL_DEPENDENCIES_RELWITHDEBINFO "${CONAN_SYSTEM_LIBS_ROBOT-CONTROL_RELWITHDEBINFO} ${CONAN_FRAMEWORKS_FOUND_ROBOT-CONTROL_RELWITHDEBINFO} CONAN_PKG::fmt CONAN_PKG::robot-model CONAN_PKG::eigen-extensions CONAN_PKG::epigraph")
    string(REPLACE " " ";" _CONAN_PKG_LIBS_ROBOT-CONTROL_DEPENDENCIES_RELWITHDEBINFO "${_CONAN_PKG_LIBS_ROBOT-CONTROL_DEPENDENCIES_RELWITHDEBINFO}")
    conan_package_library_targets("${CONAN_PKG_LIBS_ROBOT-CONTROL_RELWITHDEBINFO}" "${CONAN_LIB_DIRS_ROBOT-CONTROL_RELWITHDEBINFO}"
                                  CONAN_PACKAGE_TARGETS_ROBOT-CONTROL_RELWITHDEBINFO "${_CONAN_PKG_LIBS_ROBOT-CONTROL_DEPENDENCIES_RELWITHDEBINFO}"
                                  "relwithdebinfo" robot-control)
    set(_CONAN_PKG_LIBS_ROBOT-CONTROL_DEPENDENCIES_MINSIZEREL "${CONAN_SYSTEM_LIBS_ROBOT-CONTROL_MINSIZEREL} ${CONAN_FRAMEWORKS_FOUND_ROBOT-CONTROL_MINSIZEREL} CONAN_PKG::fmt CONAN_PKG::robot-model CONAN_PKG::eigen-extensions CONAN_PKG::epigraph")
    string(REPLACE " " ";" _CONAN_PKG_LIBS_ROBOT-CONTROL_DEPENDENCIES_MINSIZEREL "${_CONAN_PKG_LIBS_ROBOT-CONTROL_DEPENDENCIES_MINSIZEREL}")
    conan_package_library_targets("${CONAN_PKG_LIBS_ROBOT-CONTROL_MINSIZEREL}" "${CONAN_LIB_DIRS_ROBOT-CONTROL_MINSIZEREL}"
                                  CONAN_PACKAGE_TARGETS_ROBOT-CONTROL_MINSIZEREL "${_CONAN_PKG_LIBS_ROBOT-CONTROL_DEPENDENCIES_MINSIZEREL}"
                                  "minsizerel" robot-control)

    add_library(CONAN_PKG::robot-control INTERFACE IMPORTED)

    # Property INTERFACE_LINK_FLAGS do not work, necessary to add to INTERFACE_LINK_LIBRARIES
    set_property(TARGET CONAN_PKG::robot-control PROPERTY INTERFACE_LINK_LIBRARIES ${CONAN_PACKAGE_TARGETS_ROBOT-CONTROL} ${_CONAN_PKG_LIBS_ROBOT-CONTROL_DEPENDENCIES}
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_ROBOT-CONTROL_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_ROBOT-CONTROL_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:${CONAN_EXE_LINKER_FLAGS_ROBOT-CONTROL_LIST}>

                                                                 $<$<CONFIG:Release>:${CONAN_PACKAGE_TARGETS_ROBOT-CONTROL_RELEASE} ${_CONAN_PKG_LIBS_ROBOT-CONTROL_DEPENDENCIES_RELEASE}
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_ROBOT-CONTROL_RELEASE_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_ROBOT-CONTROL_RELEASE_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:${CONAN_EXE_LINKER_FLAGS_ROBOT-CONTROL_RELEASE_LIST}>>

                                                                 $<$<CONFIG:RelWithDebInfo>:${CONAN_PACKAGE_TARGETS_ROBOT-CONTROL_RELWITHDEBINFO} ${_CONAN_PKG_LIBS_ROBOT-CONTROL_DEPENDENCIES_RELWITHDEBINFO}
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_ROBOT-CONTROL_RELWITHDEBINFO_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_ROBOT-CONTROL_RELWITHDEBINFO_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:${CONAN_EXE_LINKER_FLAGS_ROBOT-CONTROL_RELWITHDEBINFO_LIST}>>

                                                                 $<$<CONFIG:MinSizeRel>:${CONAN_PACKAGE_TARGETS_ROBOT-CONTROL_MINSIZEREL} ${_CONAN_PKG_LIBS_ROBOT-CONTROL_DEPENDENCIES_MINSIZEREL}
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_ROBOT-CONTROL_MINSIZEREL_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_ROBOT-CONTROL_MINSIZEREL_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:${CONAN_EXE_LINKER_FLAGS_ROBOT-CONTROL_MINSIZEREL_LIST}>>

                                                                 $<$<CONFIG:Debug>:${CONAN_PACKAGE_TARGETS_ROBOT-CONTROL_DEBUG} ${_CONAN_PKG_LIBS_ROBOT-CONTROL_DEPENDENCIES_DEBUG}
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_ROBOT-CONTROL_DEBUG_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_ROBOT-CONTROL_DEBUG_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:${CONAN_EXE_LINKER_FLAGS_ROBOT-CONTROL_DEBUG_LIST}>>)
    set_property(TARGET CONAN_PKG::robot-control PROPERTY INTERFACE_INCLUDE_DIRECTORIES ${CONAN_INCLUDE_DIRS_ROBOT-CONTROL}
                                                                      $<$<CONFIG:Release>:${CONAN_INCLUDE_DIRS_ROBOT-CONTROL_RELEASE}>
                                                                      $<$<CONFIG:RelWithDebInfo>:${CONAN_INCLUDE_DIRS_ROBOT-CONTROL_RELWITHDEBINFO}>
                                                                      $<$<CONFIG:MinSizeRel>:${CONAN_INCLUDE_DIRS_ROBOT-CONTROL_MINSIZEREL}>
                                                                      $<$<CONFIG:Debug>:${CONAN_INCLUDE_DIRS_ROBOT-CONTROL_DEBUG}>)
    set_property(TARGET CONAN_PKG::robot-control PROPERTY INTERFACE_COMPILE_DEFINITIONS ${CONAN_COMPILE_DEFINITIONS_ROBOT-CONTROL}
                                                                      $<$<CONFIG:Release>:${CONAN_COMPILE_DEFINITIONS_ROBOT-CONTROL_RELEASE}>
                                                                      $<$<CONFIG:RelWithDebInfo>:${CONAN_COMPILE_DEFINITIONS_ROBOT-CONTROL_RELWITHDEBINFO}>
                                                                      $<$<CONFIG:MinSizeRel>:${CONAN_COMPILE_DEFINITIONS_ROBOT-CONTROL_MINSIZEREL}>
                                                                      $<$<CONFIG:Debug>:${CONAN_COMPILE_DEFINITIONS_ROBOT-CONTROL_DEBUG}>)
    set_property(TARGET CONAN_PKG::robot-control PROPERTY INTERFACE_COMPILE_OPTIONS ${CONAN_C_FLAGS_ROBOT-CONTROL_LIST} ${CONAN_CXX_FLAGS_ROBOT-CONTROL_LIST}
                                                                  $<$<CONFIG:Release>:${CONAN_C_FLAGS_ROBOT-CONTROL_RELEASE_LIST} ${CONAN_CXX_FLAGS_ROBOT-CONTROL_RELEASE_LIST}>
                                                                  $<$<CONFIG:RelWithDebInfo>:${CONAN_C_FLAGS_ROBOT-CONTROL_RELWITHDEBINFO_LIST} ${CONAN_CXX_FLAGS_ROBOT-CONTROL_RELWITHDEBINFO_LIST}>
                                                                  $<$<CONFIG:MinSizeRel>:${CONAN_C_FLAGS_ROBOT-CONTROL_MINSIZEREL_LIST} ${CONAN_CXX_FLAGS_ROBOT-CONTROL_MINSIZEREL_LIST}>
                                                                  $<$<CONFIG:Debug>:${CONAN_C_FLAGS_ROBOT-CONTROL_DEBUG_LIST}  ${CONAN_CXX_FLAGS_ROBOT-CONTROL_DEBUG_LIST}>)


    set(_CONAN_PKG_LIBS_ROBOT-MODEL_DEPENDENCIES "${CONAN_SYSTEM_LIBS_ROBOT-MODEL} ${CONAN_FRAMEWORKS_FOUND_ROBOT-MODEL} CONAN_PKG::urdfdomcpp CONAN_PKG::Eigen3 CONAN_PKG::fmt")
    string(REPLACE " " ";" _CONAN_PKG_LIBS_ROBOT-MODEL_DEPENDENCIES "${_CONAN_PKG_LIBS_ROBOT-MODEL_DEPENDENCIES}")
    conan_package_library_targets("${CONAN_PKG_LIBS_ROBOT-MODEL}" "${CONAN_LIB_DIRS_ROBOT-MODEL}"
                                  CONAN_PACKAGE_TARGETS_ROBOT-MODEL "${_CONAN_PKG_LIBS_ROBOT-MODEL_DEPENDENCIES}"
                                  "" robot-model)
    set(_CONAN_PKG_LIBS_ROBOT-MODEL_DEPENDENCIES_DEBUG "${CONAN_SYSTEM_LIBS_ROBOT-MODEL_DEBUG} ${CONAN_FRAMEWORKS_FOUND_ROBOT-MODEL_DEBUG} CONAN_PKG::urdfdomcpp CONAN_PKG::Eigen3 CONAN_PKG::fmt")
    string(REPLACE " " ";" _CONAN_PKG_LIBS_ROBOT-MODEL_DEPENDENCIES_DEBUG "${_CONAN_PKG_LIBS_ROBOT-MODEL_DEPENDENCIES_DEBUG}")
    conan_package_library_targets("${CONAN_PKG_LIBS_ROBOT-MODEL_DEBUG}" "${CONAN_LIB_DIRS_ROBOT-MODEL_DEBUG}"
                                  CONAN_PACKAGE_TARGETS_ROBOT-MODEL_DEBUG "${_CONAN_PKG_LIBS_ROBOT-MODEL_DEPENDENCIES_DEBUG}"
                                  "debug" robot-model)
    set(_CONAN_PKG_LIBS_ROBOT-MODEL_DEPENDENCIES_RELEASE "${CONAN_SYSTEM_LIBS_ROBOT-MODEL_RELEASE} ${CONAN_FRAMEWORKS_FOUND_ROBOT-MODEL_RELEASE} CONAN_PKG::urdfdomcpp CONAN_PKG::Eigen3 CONAN_PKG::fmt")
    string(REPLACE " " ";" _CONAN_PKG_LIBS_ROBOT-MODEL_DEPENDENCIES_RELEASE "${_CONAN_PKG_LIBS_ROBOT-MODEL_DEPENDENCIES_RELEASE}")
    conan_package_library_targets("${CONAN_PKG_LIBS_ROBOT-MODEL_RELEASE}" "${CONAN_LIB_DIRS_ROBOT-MODEL_RELEASE}"
                                  CONAN_PACKAGE_TARGETS_ROBOT-MODEL_RELEASE "${_CONAN_PKG_LIBS_ROBOT-MODEL_DEPENDENCIES_RELEASE}"
                                  "release" robot-model)
    set(_CONAN_PKG_LIBS_ROBOT-MODEL_DEPENDENCIES_RELWITHDEBINFO "${CONAN_SYSTEM_LIBS_ROBOT-MODEL_RELWITHDEBINFO} ${CONAN_FRAMEWORKS_FOUND_ROBOT-MODEL_RELWITHDEBINFO} CONAN_PKG::urdfdomcpp CONAN_PKG::Eigen3 CONAN_PKG::fmt")
    string(REPLACE " " ";" _CONAN_PKG_LIBS_ROBOT-MODEL_DEPENDENCIES_RELWITHDEBINFO "${_CONAN_PKG_LIBS_ROBOT-MODEL_DEPENDENCIES_RELWITHDEBINFO}")
    conan_package_library_targets("${CONAN_PKG_LIBS_ROBOT-MODEL_RELWITHDEBINFO}" "${CONAN_LIB_DIRS_ROBOT-MODEL_RELWITHDEBINFO}"
                                  CONAN_PACKAGE_TARGETS_ROBOT-MODEL_RELWITHDEBINFO "${_CONAN_PKG_LIBS_ROBOT-MODEL_DEPENDENCIES_RELWITHDEBINFO}"
                                  "relwithdebinfo" robot-model)
    set(_CONAN_PKG_LIBS_ROBOT-MODEL_DEPENDENCIES_MINSIZEREL "${CONAN_SYSTEM_LIBS_ROBOT-MODEL_MINSIZEREL} ${CONAN_FRAMEWORKS_FOUND_ROBOT-MODEL_MINSIZEREL} CONAN_PKG::urdfdomcpp CONAN_PKG::Eigen3 CONAN_PKG::fmt")
    string(REPLACE " " ";" _CONAN_PKG_LIBS_ROBOT-MODEL_DEPENDENCIES_MINSIZEREL "${_CONAN_PKG_LIBS_ROBOT-MODEL_DEPENDENCIES_MINSIZEREL}")
    conan_package_library_targets("${CONAN_PKG_LIBS_ROBOT-MODEL_MINSIZEREL}" "${CONAN_LIB_DIRS_ROBOT-MODEL_MINSIZEREL}"
                                  CONAN_PACKAGE_TARGETS_ROBOT-MODEL_MINSIZEREL "${_CONAN_PKG_LIBS_ROBOT-MODEL_DEPENDENCIES_MINSIZEREL}"
                                  "minsizerel" robot-model)

    add_library(CONAN_PKG::robot-model INTERFACE IMPORTED)

    # Property INTERFACE_LINK_FLAGS do not work, necessary to add to INTERFACE_LINK_LIBRARIES
    set_property(TARGET CONAN_PKG::robot-model PROPERTY INTERFACE_LINK_LIBRARIES ${CONAN_PACKAGE_TARGETS_ROBOT-MODEL} ${_CONAN_PKG_LIBS_ROBOT-MODEL_DEPENDENCIES}
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_ROBOT-MODEL_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_ROBOT-MODEL_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:${CONAN_EXE_LINKER_FLAGS_ROBOT-MODEL_LIST}>

                                                                 $<$<CONFIG:Release>:${CONAN_PACKAGE_TARGETS_ROBOT-MODEL_RELEASE} ${_CONAN_PKG_LIBS_ROBOT-MODEL_DEPENDENCIES_RELEASE}
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_ROBOT-MODEL_RELEASE_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_ROBOT-MODEL_RELEASE_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:${CONAN_EXE_LINKER_FLAGS_ROBOT-MODEL_RELEASE_LIST}>>

                                                                 $<$<CONFIG:RelWithDebInfo>:${CONAN_PACKAGE_TARGETS_ROBOT-MODEL_RELWITHDEBINFO} ${_CONAN_PKG_LIBS_ROBOT-MODEL_DEPENDENCIES_RELWITHDEBINFO}
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_ROBOT-MODEL_RELWITHDEBINFO_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_ROBOT-MODEL_RELWITHDEBINFO_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:${CONAN_EXE_LINKER_FLAGS_ROBOT-MODEL_RELWITHDEBINFO_LIST}>>

                                                                 $<$<CONFIG:MinSizeRel>:${CONAN_PACKAGE_TARGETS_ROBOT-MODEL_MINSIZEREL} ${_CONAN_PKG_LIBS_ROBOT-MODEL_DEPENDENCIES_MINSIZEREL}
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_ROBOT-MODEL_MINSIZEREL_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_ROBOT-MODEL_MINSIZEREL_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:${CONAN_EXE_LINKER_FLAGS_ROBOT-MODEL_MINSIZEREL_LIST}>>

                                                                 $<$<CONFIG:Debug>:${CONAN_PACKAGE_TARGETS_ROBOT-MODEL_DEBUG} ${_CONAN_PKG_LIBS_ROBOT-MODEL_DEPENDENCIES_DEBUG}
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_ROBOT-MODEL_DEBUG_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_ROBOT-MODEL_DEBUG_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:${CONAN_EXE_LINKER_FLAGS_ROBOT-MODEL_DEBUG_LIST}>>)
    set_property(TARGET CONAN_PKG::robot-model PROPERTY INTERFACE_INCLUDE_DIRECTORIES ${CONAN_INCLUDE_DIRS_ROBOT-MODEL}
                                                                      $<$<CONFIG:Release>:${CONAN_INCLUDE_DIRS_ROBOT-MODEL_RELEASE}>
                                                                      $<$<CONFIG:RelWithDebInfo>:${CONAN_INCLUDE_DIRS_ROBOT-MODEL_RELWITHDEBINFO}>
                                                                      $<$<CONFIG:MinSizeRel>:${CONAN_INCLUDE_DIRS_ROBOT-MODEL_MINSIZEREL}>
                                                                      $<$<CONFIG:Debug>:${CONAN_INCLUDE_DIRS_ROBOT-MODEL_DEBUG}>)
    set_property(TARGET CONAN_PKG::robot-model PROPERTY INTERFACE_COMPILE_DEFINITIONS ${CONAN_COMPILE_DEFINITIONS_ROBOT-MODEL}
                                                                      $<$<CONFIG:Release>:${CONAN_COMPILE_DEFINITIONS_ROBOT-MODEL_RELEASE}>
                                                                      $<$<CONFIG:RelWithDebInfo>:${CONAN_COMPILE_DEFINITIONS_ROBOT-MODEL_RELWITHDEBINFO}>
                                                                      $<$<CONFIG:MinSizeRel>:${CONAN_COMPILE_DEFINITIONS_ROBOT-MODEL_MINSIZEREL}>
                                                                      $<$<CONFIG:Debug>:${CONAN_COMPILE_DEFINITIONS_ROBOT-MODEL_DEBUG}>)
    set_property(TARGET CONAN_PKG::robot-model PROPERTY INTERFACE_COMPILE_OPTIONS ${CONAN_C_FLAGS_ROBOT-MODEL_LIST} ${CONAN_CXX_FLAGS_ROBOT-MODEL_LIST}
                                                                  $<$<CONFIG:Release>:${CONAN_C_FLAGS_ROBOT-MODEL_RELEASE_LIST} ${CONAN_CXX_FLAGS_ROBOT-MODEL_RELEASE_LIST}>
                                                                  $<$<CONFIG:RelWithDebInfo>:${CONAN_C_FLAGS_ROBOT-MODEL_RELWITHDEBINFO_LIST} ${CONAN_CXX_FLAGS_ROBOT-MODEL_RELWITHDEBINFO_LIST}>
                                                                  $<$<CONFIG:MinSizeRel>:${CONAN_C_FLAGS_ROBOT-MODEL_MINSIZEREL_LIST} ${CONAN_CXX_FLAGS_ROBOT-MODEL_MINSIZEREL_LIST}>
                                                                  $<$<CONFIG:Debug>:${CONAN_C_FLAGS_ROBOT-MODEL_DEBUG_LIST}  ${CONAN_CXX_FLAGS_ROBOT-MODEL_DEBUG_LIST}>)


    set(_CONAN_PKG_LIBS_EIGEN-EXTENSIONS_DEPENDENCIES "${CONAN_SYSTEM_LIBS_EIGEN-EXTENSIONS} ${CONAN_FRAMEWORKS_FOUND_EIGEN-EXTENSIONS} CONAN_PKG::Eigen3")
    string(REPLACE " " ";" _CONAN_PKG_LIBS_EIGEN-EXTENSIONS_DEPENDENCIES "${_CONAN_PKG_LIBS_EIGEN-EXTENSIONS_DEPENDENCIES}")
    conan_package_library_targets("${CONAN_PKG_LIBS_EIGEN-EXTENSIONS}" "${CONAN_LIB_DIRS_EIGEN-EXTENSIONS}"
                                  CONAN_PACKAGE_TARGETS_EIGEN-EXTENSIONS "${_CONAN_PKG_LIBS_EIGEN-EXTENSIONS_DEPENDENCIES}"
                                  "" eigen-extensions)
    set(_CONAN_PKG_LIBS_EIGEN-EXTENSIONS_DEPENDENCIES_DEBUG "${CONAN_SYSTEM_LIBS_EIGEN-EXTENSIONS_DEBUG} ${CONAN_FRAMEWORKS_FOUND_EIGEN-EXTENSIONS_DEBUG} CONAN_PKG::Eigen3")
    string(REPLACE " " ";" _CONAN_PKG_LIBS_EIGEN-EXTENSIONS_DEPENDENCIES_DEBUG "${_CONAN_PKG_LIBS_EIGEN-EXTENSIONS_DEPENDENCIES_DEBUG}")
    conan_package_library_targets("${CONAN_PKG_LIBS_EIGEN-EXTENSIONS_DEBUG}" "${CONAN_LIB_DIRS_EIGEN-EXTENSIONS_DEBUG}"
                                  CONAN_PACKAGE_TARGETS_EIGEN-EXTENSIONS_DEBUG "${_CONAN_PKG_LIBS_EIGEN-EXTENSIONS_DEPENDENCIES_DEBUG}"
                                  "debug" eigen-extensions)
    set(_CONAN_PKG_LIBS_EIGEN-EXTENSIONS_DEPENDENCIES_RELEASE "${CONAN_SYSTEM_LIBS_EIGEN-EXTENSIONS_RELEASE} ${CONAN_FRAMEWORKS_FOUND_EIGEN-EXTENSIONS_RELEASE} CONAN_PKG::Eigen3")
    string(REPLACE " " ";" _CONAN_PKG_LIBS_EIGEN-EXTENSIONS_DEPENDENCIES_RELEASE "${_CONAN_PKG_LIBS_EIGEN-EXTENSIONS_DEPENDENCIES_RELEASE}")
    conan_package_library_targets("${CONAN_PKG_LIBS_EIGEN-EXTENSIONS_RELEASE}" "${CONAN_LIB_DIRS_EIGEN-EXTENSIONS_RELEASE}"
                                  CONAN_PACKAGE_TARGETS_EIGEN-EXTENSIONS_RELEASE "${_CONAN_PKG_LIBS_EIGEN-EXTENSIONS_DEPENDENCIES_RELEASE}"
                                  "release" eigen-extensions)
    set(_CONAN_PKG_LIBS_EIGEN-EXTENSIONS_DEPENDENCIES_RELWITHDEBINFO "${CONAN_SYSTEM_LIBS_EIGEN-EXTENSIONS_RELWITHDEBINFO} ${CONAN_FRAMEWORKS_FOUND_EIGEN-EXTENSIONS_RELWITHDEBINFO} CONAN_PKG::Eigen3")
    string(REPLACE " " ";" _CONAN_PKG_LIBS_EIGEN-EXTENSIONS_DEPENDENCIES_RELWITHDEBINFO "${_CONAN_PKG_LIBS_EIGEN-EXTENSIONS_DEPENDENCIES_RELWITHDEBINFO}")
    conan_package_library_targets("${CONAN_PKG_LIBS_EIGEN-EXTENSIONS_RELWITHDEBINFO}" "${CONAN_LIB_DIRS_EIGEN-EXTENSIONS_RELWITHDEBINFO}"
                                  CONAN_PACKAGE_TARGETS_EIGEN-EXTENSIONS_RELWITHDEBINFO "${_CONAN_PKG_LIBS_EIGEN-EXTENSIONS_DEPENDENCIES_RELWITHDEBINFO}"
                                  "relwithdebinfo" eigen-extensions)
    set(_CONAN_PKG_LIBS_EIGEN-EXTENSIONS_DEPENDENCIES_MINSIZEREL "${CONAN_SYSTEM_LIBS_EIGEN-EXTENSIONS_MINSIZEREL} ${CONAN_FRAMEWORKS_FOUND_EIGEN-EXTENSIONS_MINSIZEREL} CONAN_PKG::Eigen3")
    string(REPLACE " " ";" _CONAN_PKG_LIBS_EIGEN-EXTENSIONS_DEPENDENCIES_MINSIZEREL "${_CONAN_PKG_LIBS_EIGEN-EXTENSIONS_DEPENDENCIES_MINSIZEREL}")
    conan_package_library_targets("${CONAN_PKG_LIBS_EIGEN-EXTENSIONS_MINSIZEREL}" "${CONAN_LIB_DIRS_EIGEN-EXTENSIONS_MINSIZEREL}"
                                  CONAN_PACKAGE_TARGETS_EIGEN-EXTENSIONS_MINSIZEREL "${_CONAN_PKG_LIBS_EIGEN-EXTENSIONS_DEPENDENCIES_MINSIZEREL}"
                                  "minsizerel" eigen-extensions)

    add_library(CONAN_PKG::eigen-extensions INTERFACE IMPORTED)

    # Property INTERFACE_LINK_FLAGS do not work, necessary to add to INTERFACE_LINK_LIBRARIES
    set_property(TARGET CONAN_PKG::eigen-extensions PROPERTY INTERFACE_LINK_LIBRARIES ${CONAN_PACKAGE_TARGETS_EIGEN-EXTENSIONS} ${_CONAN_PKG_LIBS_EIGEN-EXTENSIONS_DEPENDENCIES}
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_EIGEN-EXTENSIONS_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_EIGEN-EXTENSIONS_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:${CONAN_EXE_LINKER_FLAGS_EIGEN-EXTENSIONS_LIST}>

                                                                 $<$<CONFIG:Release>:${CONAN_PACKAGE_TARGETS_EIGEN-EXTENSIONS_RELEASE} ${_CONAN_PKG_LIBS_EIGEN-EXTENSIONS_DEPENDENCIES_RELEASE}
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_EIGEN-EXTENSIONS_RELEASE_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_EIGEN-EXTENSIONS_RELEASE_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:${CONAN_EXE_LINKER_FLAGS_EIGEN-EXTENSIONS_RELEASE_LIST}>>

                                                                 $<$<CONFIG:RelWithDebInfo>:${CONAN_PACKAGE_TARGETS_EIGEN-EXTENSIONS_RELWITHDEBINFO} ${_CONAN_PKG_LIBS_EIGEN-EXTENSIONS_DEPENDENCIES_RELWITHDEBINFO}
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_EIGEN-EXTENSIONS_RELWITHDEBINFO_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_EIGEN-EXTENSIONS_RELWITHDEBINFO_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:${CONAN_EXE_LINKER_FLAGS_EIGEN-EXTENSIONS_RELWITHDEBINFO_LIST}>>

                                                                 $<$<CONFIG:MinSizeRel>:${CONAN_PACKAGE_TARGETS_EIGEN-EXTENSIONS_MINSIZEREL} ${_CONAN_PKG_LIBS_EIGEN-EXTENSIONS_DEPENDENCIES_MINSIZEREL}
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_EIGEN-EXTENSIONS_MINSIZEREL_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_EIGEN-EXTENSIONS_MINSIZEREL_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:${CONAN_EXE_LINKER_FLAGS_EIGEN-EXTENSIONS_MINSIZEREL_LIST}>>

                                                                 $<$<CONFIG:Debug>:${CONAN_PACKAGE_TARGETS_EIGEN-EXTENSIONS_DEBUG} ${_CONAN_PKG_LIBS_EIGEN-EXTENSIONS_DEPENDENCIES_DEBUG}
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_EIGEN-EXTENSIONS_DEBUG_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_EIGEN-EXTENSIONS_DEBUG_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:${CONAN_EXE_LINKER_FLAGS_EIGEN-EXTENSIONS_DEBUG_LIST}>>)
    set_property(TARGET CONAN_PKG::eigen-extensions PROPERTY INTERFACE_INCLUDE_DIRECTORIES ${CONAN_INCLUDE_DIRS_EIGEN-EXTENSIONS}
                                                                      $<$<CONFIG:Release>:${CONAN_INCLUDE_DIRS_EIGEN-EXTENSIONS_RELEASE}>
                                                                      $<$<CONFIG:RelWithDebInfo>:${CONAN_INCLUDE_DIRS_EIGEN-EXTENSIONS_RELWITHDEBINFO}>
                                                                      $<$<CONFIG:MinSizeRel>:${CONAN_INCLUDE_DIRS_EIGEN-EXTENSIONS_MINSIZEREL}>
                                                                      $<$<CONFIG:Debug>:${CONAN_INCLUDE_DIRS_EIGEN-EXTENSIONS_DEBUG}>)
    set_property(TARGET CONAN_PKG::eigen-extensions PROPERTY INTERFACE_COMPILE_DEFINITIONS ${CONAN_COMPILE_DEFINITIONS_EIGEN-EXTENSIONS}
                                                                      $<$<CONFIG:Release>:${CONAN_COMPILE_DEFINITIONS_EIGEN-EXTENSIONS_RELEASE}>
                                                                      $<$<CONFIG:RelWithDebInfo>:${CONAN_COMPILE_DEFINITIONS_EIGEN-EXTENSIONS_RELWITHDEBINFO}>
                                                                      $<$<CONFIG:MinSizeRel>:${CONAN_COMPILE_DEFINITIONS_EIGEN-EXTENSIONS_MINSIZEREL}>
                                                                      $<$<CONFIG:Debug>:${CONAN_COMPILE_DEFINITIONS_EIGEN-EXTENSIONS_DEBUG}>)
    set_property(TARGET CONAN_PKG::eigen-extensions PROPERTY INTERFACE_COMPILE_OPTIONS ${CONAN_C_FLAGS_EIGEN-EXTENSIONS_LIST} ${CONAN_CXX_FLAGS_EIGEN-EXTENSIONS_LIST}
                                                                  $<$<CONFIG:Release>:${CONAN_C_FLAGS_EIGEN-EXTENSIONS_RELEASE_LIST} ${CONAN_CXX_FLAGS_EIGEN-EXTENSIONS_RELEASE_LIST}>
                                                                  $<$<CONFIG:RelWithDebInfo>:${CONAN_C_FLAGS_EIGEN-EXTENSIONS_RELWITHDEBINFO_LIST} ${CONAN_CXX_FLAGS_EIGEN-EXTENSIONS_RELWITHDEBINFO_LIST}>
                                                                  $<$<CONFIG:MinSizeRel>:${CONAN_C_FLAGS_EIGEN-EXTENSIONS_MINSIZEREL_LIST} ${CONAN_CXX_FLAGS_EIGEN-EXTENSIONS_MINSIZEREL_LIST}>
                                                                  $<$<CONFIG:Debug>:${CONAN_C_FLAGS_EIGEN-EXTENSIONS_DEBUG_LIST}  ${CONAN_CXX_FLAGS_EIGEN-EXTENSIONS_DEBUG_LIST}>)


    set(_CONAN_PKG_LIBS_EPIGRAPH_DEPENDENCIES "${CONAN_SYSTEM_LIBS_EPIGRAPH} ${CONAN_FRAMEWORKS_FOUND_EPIGRAPH} CONAN_PKG::Eigen3 CONAN_PKG::osqp CONAN_PKG::ecos")
    string(REPLACE " " ";" _CONAN_PKG_LIBS_EPIGRAPH_DEPENDENCIES "${_CONAN_PKG_LIBS_EPIGRAPH_DEPENDENCIES}")
    conan_package_library_targets("${CONAN_PKG_LIBS_EPIGRAPH}" "${CONAN_LIB_DIRS_EPIGRAPH}"
                                  CONAN_PACKAGE_TARGETS_EPIGRAPH "${_CONAN_PKG_LIBS_EPIGRAPH_DEPENDENCIES}"
                                  "" epigraph)
    set(_CONAN_PKG_LIBS_EPIGRAPH_DEPENDENCIES_DEBUG "${CONAN_SYSTEM_LIBS_EPIGRAPH_DEBUG} ${CONAN_FRAMEWORKS_FOUND_EPIGRAPH_DEBUG} CONAN_PKG::Eigen3 CONAN_PKG::osqp CONAN_PKG::ecos")
    string(REPLACE " " ";" _CONAN_PKG_LIBS_EPIGRAPH_DEPENDENCIES_DEBUG "${_CONAN_PKG_LIBS_EPIGRAPH_DEPENDENCIES_DEBUG}")
    conan_package_library_targets("${CONAN_PKG_LIBS_EPIGRAPH_DEBUG}" "${CONAN_LIB_DIRS_EPIGRAPH_DEBUG}"
                                  CONAN_PACKAGE_TARGETS_EPIGRAPH_DEBUG "${_CONAN_PKG_LIBS_EPIGRAPH_DEPENDENCIES_DEBUG}"
                                  "debug" epigraph)
    set(_CONAN_PKG_LIBS_EPIGRAPH_DEPENDENCIES_RELEASE "${CONAN_SYSTEM_LIBS_EPIGRAPH_RELEASE} ${CONAN_FRAMEWORKS_FOUND_EPIGRAPH_RELEASE} CONAN_PKG::Eigen3 CONAN_PKG::osqp CONAN_PKG::ecos")
    string(REPLACE " " ";" _CONAN_PKG_LIBS_EPIGRAPH_DEPENDENCIES_RELEASE "${_CONAN_PKG_LIBS_EPIGRAPH_DEPENDENCIES_RELEASE}")
    conan_package_library_targets("${CONAN_PKG_LIBS_EPIGRAPH_RELEASE}" "${CONAN_LIB_DIRS_EPIGRAPH_RELEASE}"
                                  CONAN_PACKAGE_TARGETS_EPIGRAPH_RELEASE "${_CONAN_PKG_LIBS_EPIGRAPH_DEPENDENCIES_RELEASE}"
                                  "release" epigraph)
    set(_CONAN_PKG_LIBS_EPIGRAPH_DEPENDENCIES_RELWITHDEBINFO "${CONAN_SYSTEM_LIBS_EPIGRAPH_RELWITHDEBINFO} ${CONAN_FRAMEWORKS_FOUND_EPIGRAPH_RELWITHDEBINFO} CONAN_PKG::Eigen3 CONAN_PKG::osqp CONAN_PKG::ecos")
    string(REPLACE " " ";" _CONAN_PKG_LIBS_EPIGRAPH_DEPENDENCIES_RELWITHDEBINFO "${_CONAN_PKG_LIBS_EPIGRAPH_DEPENDENCIES_RELWITHDEBINFO}")
    conan_package_library_targets("${CONAN_PKG_LIBS_EPIGRAPH_RELWITHDEBINFO}" "${CONAN_LIB_DIRS_EPIGRAPH_RELWITHDEBINFO}"
                                  CONAN_PACKAGE_TARGETS_EPIGRAPH_RELWITHDEBINFO "${_CONAN_PKG_LIBS_EPIGRAPH_DEPENDENCIES_RELWITHDEBINFO}"
                                  "relwithdebinfo" epigraph)
    set(_CONAN_PKG_LIBS_EPIGRAPH_DEPENDENCIES_MINSIZEREL "${CONAN_SYSTEM_LIBS_EPIGRAPH_MINSIZEREL} ${CONAN_FRAMEWORKS_FOUND_EPIGRAPH_MINSIZEREL} CONAN_PKG::Eigen3 CONAN_PKG::osqp CONAN_PKG::ecos")
    string(REPLACE " " ";" _CONAN_PKG_LIBS_EPIGRAPH_DEPENDENCIES_MINSIZEREL "${_CONAN_PKG_LIBS_EPIGRAPH_DEPENDENCIES_MINSIZEREL}")
    conan_package_library_targets("${CONAN_PKG_LIBS_EPIGRAPH_MINSIZEREL}" "${CONAN_LIB_DIRS_EPIGRAPH_MINSIZEREL}"
                                  CONAN_PACKAGE_TARGETS_EPIGRAPH_MINSIZEREL "${_CONAN_PKG_LIBS_EPIGRAPH_DEPENDENCIES_MINSIZEREL}"
                                  "minsizerel" epigraph)

    add_library(CONAN_PKG::epigraph INTERFACE IMPORTED)

    # Property INTERFACE_LINK_FLAGS do not work, necessary to add to INTERFACE_LINK_LIBRARIES
    set_property(TARGET CONAN_PKG::epigraph PROPERTY INTERFACE_LINK_LIBRARIES ${CONAN_PACKAGE_TARGETS_EPIGRAPH} ${_CONAN_PKG_LIBS_EPIGRAPH_DEPENDENCIES}
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_EPIGRAPH_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_EPIGRAPH_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:${CONAN_EXE_LINKER_FLAGS_EPIGRAPH_LIST}>

                                                                 $<$<CONFIG:Release>:${CONAN_PACKAGE_TARGETS_EPIGRAPH_RELEASE} ${_CONAN_PKG_LIBS_EPIGRAPH_DEPENDENCIES_RELEASE}
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_EPIGRAPH_RELEASE_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_EPIGRAPH_RELEASE_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:${CONAN_EXE_LINKER_FLAGS_EPIGRAPH_RELEASE_LIST}>>

                                                                 $<$<CONFIG:RelWithDebInfo>:${CONAN_PACKAGE_TARGETS_EPIGRAPH_RELWITHDEBINFO} ${_CONAN_PKG_LIBS_EPIGRAPH_DEPENDENCIES_RELWITHDEBINFO}
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_EPIGRAPH_RELWITHDEBINFO_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_EPIGRAPH_RELWITHDEBINFO_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:${CONAN_EXE_LINKER_FLAGS_EPIGRAPH_RELWITHDEBINFO_LIST}>>

                                                                 $<$<CONFIG:MinSizeRel>:${CONAN_PACKAGE_TARGETS_EPIGRAPH_MINSIZEREL} ${_CONAN_PKG_LIBS_EPIGRAPH_DEPENDENCIES_MINSIZEREL}
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_EPIGRAPH_MINSIZEREL_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_EPIGRAPH_MINSIZEREL_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:${CONAN_EXE_LINKER_FLAGS_EPIGRAPH_MINSIZEREL_LIST}>>

                                                                 $<$<CONFIG:Debug>:${CONAN_PACKAGE_TARGETS_EPIGRAPH_DEBUG} ${_CONAN_PKG_LIBS_EPIGRAPH_DEPENDENCIES_DEBUG}
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_EPIGRAPH_DEBUG_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_EPIGRAPH_DEBUG_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:${CONAN_EXE_LINKER_FLAGS_EPIGRAPH_DEBUG_LIST}>>)
    set_property(TARGET CONAN_PKG::epigraph PROPERTY INTERFACE_INCLUDE_DIRECTORIES ${CONAN_INCLUDE_DIRS_EPIGRAPH}
                                                                      $<$<CONFIG:Release>:${CONAN_INCLUDE_DIRS_EPIGRAPH_RELEASE}>
                                                                      $<$<CONFIG:RelWithDebInfo>:${CONAN_INCLUDE_DIRS_EPIGRAPH_RELWITHDEBINFO}>
                                                                      $<$<CONFIG:MinSizeRel>:${CONAN_INCLUDE_DIRS_EPIGRAPH_MINSIZEREL}>
                                                                      $<$<CONFIG:Debug>:${CONAN_INCLUDE_DIRS_EPIGRAPH_DEBUG}>)
    set_property(TARGET CONAN_PKG::epigraph PROPERTY INTERFACE_COMPILE_DEFINITIONS ${CONAN_COMPILE_DEFINITIONS_EPIGRAPH}
                                                                      $<$<CONFIG:Release>:${CONAN_COMPILE_DEFINITIONS_EPIGRAPH_RELEASE}>
                                                                      $<$<CONFIG:RelWithDebInfo>:${CONAN_COMPILE_DEFINITIONS_EPIGRAPH_RELWITHDEBINFO}>
                                                                      $<$<CONFIG:MinSizeRel>:${CONAN_COMPILE_DEFINITIONS_EPIGRAPH_MINSIZEREL}>
                                                                      $<$<CONFIG:Debug>:${CONAN_COMPILE_DEFINITIONS_EPIGRAPH_DEBUG}>)
    set_property(TARGET CONAN_PKG::epigraph PROPERTY INTERFACE_COMPILE_OPTIONS ${CONAN_C_FLAGS_EPIGRAPH_LIST} ${CONAN_CXX_FLAGS_EPIGRAPH_LIST}
                                                                  $<$<CONFIG:Release>:${CONAN_C_FLAGS_EPIGRAPH_RELEASE_LIST} ${CONAN_CXX_FLAGS_EPIGRAPH_RELEASE_LIST}>
                                                                  $<$<CONFIG:RelWithDebInfo>:${CONAN_C_FLAGS_EPIGRAPH_RELWITHDEBINFO_LIST} ${CONAN_CXX_FLAGS_EPIGRAPH_RELWITHDEBINFO_LIST}>
                                                                  $<$<CONFIG:MinSizeRel>:${CONAN_C_FLAGS_EPIGRAPH_MINSIZEREL_LIST} ${CONAN_CXX_FLAGS_EPIGRAPH_MINSIZEREL_LIST}>
                                                                  $<$<CONFIG:Debug>:${CONAN_C_FLAGS_EPIGRAPH_DEBUG_LIST}  ${CONAN_CXX_FLAGS_EPIGRAPH_DEBUG_LIST}>)


    set(_CONAN_PKG_LIBS_URDFDOMCPP_DEPENDENCIES "${CONAN_SYSTEM_LIBS_URDFDOMCPP} ${CONAN_FRAMEWORKS_FOUND_URDFDOMCPP} CONAN_PKG::tinyxml2")
    string(REPLACE " " ";" _CONAN_PKG_LIBS_URDFDOMCPP_DEPENDENCIES "${_CONAN_PKG_LIBS_URDFDOMCPP_DEPENDENCIES}")
    conan_package_library_targets("${CONAN_PKG_LIBS_URDFDOMCPP}" "${CONAN_LIB_DIRS_URDFDOMCPP}"
                                  CONAN_PACKAGE_TARGETS_URDFDOMCPP "${_CONAN_PKG_LIBS_URDFDOMCPP_DEPENDENCIES}"
                                  "" urdfdomcpp)
    set(_CONAN_PKG_LIBS_URDFDOMCPP_DEPENDENCIES_DEBUG "${CONAN_SYSTEM_LIBS_URDFDOMCPP_DEBUG} ${CONAN_FRAMEWORKS_FOUND_URDFDOMCPP_DEBUG} CONAN_PKG::tinyxml2")
    string(REPLACE " " ";" _CONAN_PKG_LIBS_URDFDOMCPP_DEPENDENCIES_DEBUG "${_CONAN_PKG_LIBS_URDFDOMCPP_DEPENDENCIES_DEBUG}")
    conan_package_library_targets("${CONAN_PKG_LIBS_URDFDOMCPP_DEBUG}" "${CONAN_LIB_DIRS_URDFDOMCPP_DEBUG}"
                                  CONAN_PACKAGE_TARGETS_URDFDOMCPP_DEBUG "${_CONAN_PKG_LIBS_URDFDOMCPP_DEPENDENCIES_DEBUG}"
                                  "debug" urdfdomcpp)
    set(_CONAN_PKG_LIBS_URDFDOMCPP_DEPENDENCIES_RELEASE "${CONAN_SYSTEM_LIBS_URDFDOMCPP_RELEASE} ${CONAN_FRAMEWORKS_FOUND_URDFDOMCPP_RELEASE} CONAN_PKG::tinyxml2")
    string(REPLACE " " ";" _CONAN_PKG_LIBS_URDFDOMCPP_DEPENDENCIES_RELEASE "${_CONAN_PKG_LIBS_URDFDOMCPP_DEPENDENCIES_RELEASE}")
    conan_package_library_targets("${CONAN_PKG_LIBS_URDFDOMCPP_RELEASE}" "${CONAN_LIB_DIRS_URDFDOMCPP_RELEASE}"
                                  CONAN_PACKAGE_TARGETS_URDFDOMCPP_RELEASE "${_CONAN_PKG_LIBS_URDFDOMCPP_DEPENDENCIES_RELEASE}"
                                  "release" urdfdomcpp)
    set(_CONAN_PKG_LIBS_URDFDOMCPP_DEPENDENCIES_RELWITHDEBINFO "${CONAN_SYSTEM_LIBS_URDFDOMCPP_RELWITHDEBINFO} ${CONAN_FRAMEWORKS_FOUND_URDFDOMCPP_RELWITHDEBINFO} CONAN_PKG::tinyxml2")
    string(REPLACE " " ";" _CONAN_PKG_LIBS_URDFDOMCPP_DEPENDENCIES_RELWITHDEBINFO "${_CONAN_PKG_LIBS_URDFDOMCPP_DEPENDENCIES_RELWITHDEBINFO}")
    conan_package_library_targets("${CONAN_PKG_LIBS_URDFDOMCPP_RELWITHDEBINFO}" "${CONAN_LIB_DIRS_URDFDOMCPP_RELWITHDEBINFO}"
                                  CONAN_PACKAGE_TARGETS_URDFDOMCPP_RELWITHDEBINFO "${_CONAN_PKG_LIBS_URDFDOMCPP_DEPENDENCIES_RELWITHDEBINFO}"
                                  "relwithdebinfo" urdfdomcpp)
    set(_CONAN_PKG_LIBS_URDFDOMCPP_DEPENDENCIES_MINSIZEREL "${CONAN_SYSTEM_LIBS_URDFDOMCPP_MINSIZEREL} ${CONAN_FRAMEWORKS_FOUND_URDFDOMCPP_MINSIZEREL} CONAN_PKG::tinyxml2")
    string(REPLACE " " ";" _CONAN_PKG_LIBS_URDFDOMCPP_DEPENDENCIES_MINSIZEREL "${_CONAN_PKG_LIBS_URDFDOMCPP_DEPENDENCIES_MINSIZEREL}")
    conan_package_library_targets("${CONAN_PKG_LIBS_URDFDOMCPP_MINSIZEREL}" "${CONAN_LIB_DIRS_URDFDOMCPP_MINSIZEREL}"
                                  CONAN_PACKAGE_TARGETS_URDFDOMCPP_MINSIZEREL "${_CONAN_PKG_LIBS_URDFDOMCPP_DEPENDENCIES_MINSIZEREL}"
                                  "minsizerel" urdfdomcpp)

    add_library(CONAN_PKG::urdfdomcpp INTERFACE IMPORTED)

    # Property INTERFACE_LINK_FLAGS do not work, necessary to add to INTERFACE_LINK_LIBRARIES
    set_property(TARGET CONAN_PKG::urdfdomcpp PROPERTY INTERFACE_LINK_LIBRARIES ${CONAN_PACKAGE_TARGETS_URDFDOMCPP} ${_CONAN_PKG_LIBS_URDFDOMCPP_DEPENDENCIES}
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_URDFDOMCPP_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_URDFDOMCPP_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:${CONAN_EXE_LINKER_FLAGS_URDFDOMCPP_LIST}>

                                                                 $<$<CONFIG:Release>:${CONAN_PACKAGE_TARGETS_URDFDOMCPP_RELEASE} ${_CONAN_PKG_LIBS_URDFDOMCPP_DEPENDENCIES_RELEASE}
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_URDFDOMCPP_RELEASE_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_URDFDOMCPP_RELEASE_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:${CONAN_EXE_LINKER_FLAGS_URDFDOMCPP_RELEASE_LIST}>>

                                                                 $<$<CONFIG:RelWithDebInfo>:${CONAN_PACKAGE_TARGETS_URDFDOMCPP_RELWITHDEBINFO} ${_CONAN_PKG_LIBS_URDFDOMCPP_DEPENDENCIES_RELWITHDEBINFO}
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_URDFDOMCPP_RELWITHDEBINFO_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_URDFDOMCPP_RELWITHDEBINFO_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:${CONAN_EXE_LINKER_FLAGS_URDFDOMCPP_RELWITHDEBINFO_LIST}>>

                                                                 $<$<CONFIG:MinSizeRel>:${CONAN_PACKAGE_TARGETS_URDFDOMCPP_MINSIZEREL} ${_CONAN_PKG_LIBS_URDFDOMCPP_DEPENDENCIES_MINSIZEREL}
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_URDFDOMCPP_MINSIZEREL_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_URDFDOMCPP_MINSIZEREL_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:${CONAN_EXE_LINKER_FLAGS_URDFDOMCPP_MINSIZEREL_LIST}>>

                                                                 $<$<CONFIG:Debug>:${CONAN_PACKAGE_TARGETS_URDFDOMCPP_DEBUG} ${_CONAN_PKG_LIBS_URDFDOMCPP_DEPENDENCIES_DEBUG}
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_URDFDOMCPP_DEBUG_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_URDFDOMCPP_DEBUG_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:${CONAN_EXE_LINKER_FLAGS_URDFDOMCPP_DEBUG_LIST}>>)
    set_property(TARGET CONAN_PKG::urdfdomcpp PROPERTY INTERFACE_INCLUDE_DIRECTORIES ${CONAN_INCLUDE_DIRS_URDFDOMCPP}
                                                                      $<$<CONFIG:Release>:${CONAN_INCLUDE_DIRS_URDFDOMCPP_RELEASE}>
                                                                      $<$<CONFIG:RelWithDebInfo>:${CONAN_INCLUDE_DIRS_URDFDOMCPP_RELWITHDEBINFO}>
                                                                      $<$<CONFIG:MinSizeRel>:${CONAN_INCLUDE_DIRS_URDFDOMCPP_MINSIZEREL}>
                                                                      $<$<CONFIG:Debug>:${CONAN_INCLUDE_DIRS_URDFDOMCPP_DEBUG}>)
    set_property(TARGET CONAN_PKG::urdfdomcpp PROPERTY INTERFACE_COMPILE_DEFINITIONS ${CONAN_COMPILE_DEFINITIONS_URDFDOMCPP}
                                                                      $<$<CONFIG:Release>:${CONAN_COMPILE_DEFINITIONS_URDFDOMCPP_RELEASE}>
                                                                      $<$<CONFIG:RelWithDebInfo>:${CONAN_COMPILE_DEFINITIONS_URDFDOMCPP_RELWITHDEBINFO}>
                                                                      $<$<CONFIG:MinSizeRel>:${CONAN_COMPILE_DEFINITIONS_URDFDOMCPP_MINSIZEREL}>
                                                                      $<$<CONFIG:Debug>:${CONAN_COMPILE_DEFINITIONS_URDFDOMCPP_DEBUG}>)
    set_property(TARGET CONAN_PKG::urdfdomcpp PROPERTY INTERFACE_COMPILE_OPTIONS ${CONAN_C_FLAGS_URDFDOMCPP_LIST} ${CONAN_CXX_FLAGS_URDFDOMCPP_LIST}
                                                                  $<$<CONFIG:Release>:${CONAN_C_FLAGS_URDFDOMCPP_RELEASE_LIST} ${CONAN_CXX_FLAGS_URDFDOMCPP_RELEASE_LIST}>
                                                                  $<$<CONFIG:RelWithDebInfo>:${CONAN_C_FLAGS_URDFDOMCPP_RELWITHDEBINFO_LIST} ${CONAN_CXX_FLAGS_URDFDOMCPP_RELWITHDEBINFO_LIST}>
                                                                  $<$<CONFIG:MinSizeRel>:${CONAN_C_FLAGS_URDFDOMCPP_MINSIZEREL_LIST} ${CONAN_CXX_FLAGS_URDFDOMCPP_MINSIZEREL_LIST}>
                                                                  $<$<CONFIG:Debug>:${CONAN_C_FLAGS_URDFDOMCPP_DEBUG_LIST}  ${CONAN_CXX_FLAGS_URDFDOMCPP_DEBUG_LIST}>)


    set(_CONAN_PKG_LIBS_EIGEN3_DEPENDENCIES "${CONAN_SYSTEM_LIBS_EIGEN3} ${CONAN_FRAMEWORKS_FOUND_EIGEN3} ")
    string(REPLACE " " ";" _CONAN_PKG_LIBS_EIGEN3_DEPENDENCIES "${_CONAN_PKG_LIBS_EIGEN3_DEPENDENCIES}")
    conan_package_library_targets("${CONAN_PKG_LIBS_EIGEN3}" "${CONAN_LIB_DIRS_EIGEN3}"
                                  CONAN_PACKAGE_TARGETS_EIGEN3 "${_CONAN_PKG_LIBS_EIGEN3_DEPENDENCIES}"
                                  "" Eigen3)
    set(_CONAN_PKG_LIBS_EIGEN3_DEPENDENCIES_DEBUG "${CONAN_SYSTEM_LIBS_EIGEN3_DEBUG} ${CONAN_FRAMEWORKS_FOUND_EIGEN3_DEBUG} ")
    string(REPLACE " " ";" _CONAN_PKG_LIBS_EIGEN3_DEPENDENCIES_DEBUG "${_CONAN_PKG_LIBS_EIGEN3_DEPENDENCIES_DEBUG}")
    conan_package_library_targets("${CONAN_PKG_LIBS_EIGEN3_DEBUG}" "${CONAN_LIB_DIRS_EIGEN3_DEBUG}"
                                  CONAN_PACKAGE_TARGETS_EIGEN3_DEBUG "${_CONAN_PKG_LIBS_EIGEN3_DEPENDENCIES_DEBUG}"
                                  "debug" Eigen3)
    set(_CONAN_PKG_LIBS_EIGEN3_DEPENDENCIES_RELEASE "${CONAN_SYSTEM_LIBS_EIGEN3_RELEASE} ${CONAN_FRAMEWORKS_FOUND_EIGEN3_RELEASE} ")
    string(REPLACE " " ";" _CONAN_PKG_LIBS_EIGEN3_DEPENDENCIES_RELEASE "${_CONAN_PKG_LIBS_EIGEN3_DEPENDENCIES_RELEASE}")
    conan_package_library_targets("${CONAN_PKG_LIBS_EIGEN3_RELEASE}" "${CONAN_LIB_DIRS_EIGEN3_RELEASE}"
                                  CONAN_PACKAGE_TARGETS_EIGEN3_RELEASE "${_CONAN_PKG_LIBS_EIGEN3_DEPENDENCIES_RELEASE}"
                                  "release" Eigen3)
    set(_CONAN_PKG_LIBS_EIGEN3_DEPENDENCIES_RELWITHDEBINFO "${CONAN_SYSTEM_LIBS_EIGEN3_RELWITHDEBINFO} ${CONAN_FRAMEWORKS_FOUND_EIGEN3_RELWITHDEBINFO} ")
    string(REPLACE " " ";" _CONAN_PKG_LIBS_EIGEN3_DEPENDENCIES_RELWITHDEBINFO "${_CONAN_PKG_LIBS_EIGEN3_DEPENDENCIES_RELWITHDEBINFO}")
    conan_package_library_targets("${CONAN_PKG_LIBS_EIGEN3_RELWITHDEBINFO}" "${CONAN_LIB_DIRS_EIGEN3_RELWITHDEBINFO}"
                                  CONAN_PACKAGE_TARGETS_EIGEN3_RELWITHDEBINFO "${_CONAN_PKG_LIBS_EIGEN3_DEPENDENCIES_RELWITHDEBINFO}"
                                  "relwithdebinfo" Eigen3)
    set(_CONAN_PKG_LIBS_EIGEN3_DEPENDENCIES_MINSIZEREL "${CONAN_SYSTEM_LIBS_EIGEN3_MINSIZEREL} ${CONAN_FRAMEWORKS_FOUND_EIGEN3_MINSIZEREL} ")
    string(REPLACE " " ";" _CONAN_PKG_LIBS_EIGEN3_DEPENDENCIES_MINSIZEREL "${_CONAN_PKG_LIBS_EIGEN3_DEPENDENCIES_MINSIZEREL}")
    conan_package_library_targets("${CONAN_PKG_LIBS_EIGEN3_MINSIZEREL}" "${CONAN_LIB_DIRS_EIGEN3_MINSIZEREL}"
                                  CONAN_PACKAGE_TARGETS_EIGEN3_MINSIZEREL "${_CONAN_PKG_LIBS_EIGEN3_DEPENDENCIES_MINSIZEREL}"
                                  "minsizerel" Eigen3)

    add_library(CONAN_PKG::Eigen3 INTERFACE IMPORTED)

    # Property INTERFACE_LINK_FLAGS do not work, necessary to add to INTERFACE_LINK_LIBRARIES
    set_property(TARGET CONAN_PKG::Eigen3 PROPERTY INTERFACE_LINK_LIBRARIES ${CONAN_PACKAGE_TARGETS_EIGEN3} ${_CONAN_PKG_LIBS_EIGEN3_DEPENDENCIES}
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_EIGEN3_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_EIGEN3_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:${CONAN_EXE_LINKER_FLAGS_EIGEN3_LIST}>

                                                                 $<$<CONFIG:Release>:${CONAN_PACKAGE_TARGETS_EIGEN3_RELEASE} ${_CONAN_PKG_LIBS_EIGEN3_DEPENDENCIES_RELEASE}
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_EIGEN3_RELEASE_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_EIGEN3_RELEASE_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:${CONAN_EXE_LINKER_FLAGS_EIGEN3_RELEASE_LIST}>>

                                                                 $<$<CONFIG:RelWithDebInfo>:${CONAN_PACKAGE_TARGETS_EIGEN3_RELWITHDEBINFO} ${_CONAN_PKG_LIBS_EIGEN3_DEPENDENCIES_RELWITHDEBINFO}
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_EIGEN3_RELWITHDEBINFO_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_EIGEN3_RELWITHDEBINFO_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:${CONAN_EXE_LINKER_FLAGS_EIGEN3_RELWITHDEBINFO_LIST}>>

                                                                 $<$<CONFIG:MinSizeRel>:${CONAN_PACKAGE_TARGETS_EIGEN3_MINSIZEREL} ${_CONAN_PKG_LIBS_EIGEN3_DEPENDENCIES_MINSIZEREL}
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_EIGEN3_MINSIZEREL_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_EIGEN3_MINSIZEREL_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:${CONAN_EXE_LINKER_FLAGS_EIGEN3_MINSIZEREL_LIST}>>

                                                                 $<$<CONFIG:Debug>:${CONAN_PACKAGE_TARGETS_EIGEN3_DEBUG} ${_CONAN_PKG_LIBS_EIGEN3_DEPENDENCIES_DEBUG}
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_EIGEN3_DEBUG_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_EIGEN3_DEBUG_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:${CONAN_EXE_LINKER_FLAGS_EIGEN3_DEBUG_LIST}>>)
    set_property(TARGET CONAN_PKG::Eigen3 PROPERTY INTERFACE_INCLUDE_DIRECTORIES ${CONAN_INCLUDE_DIRS_EIGEN3}
                                                                      $<$<CONFIG:Release>:${CONAN_INCLUDE_DIRS_EIGEN3_RELEASE}>
                                                                      $<$<CONFIG:RelWithDebInfo>:${CONAN_INCLUDE_DIRS_EIGEN3_RELWITHDEBINFO}>
                                                                      $<$<CONFIG:MinSizeRel>:${CONAN_INCLUDE_DIRS_EIGEN3_MINSIZEREL}>
                                                                      $<$<CONFIG:Debug>:${CONAN_INCLUDE_DIRS_EIGEN3_DEBUG}>)
    set_property(TARGET CONAN_PKG::Eigen3 PROPERTY INTERFACE_COMPILE_DEFINITIONS ${CONAN_COMPILE_DEFINITIONS_EIGEN3}
                                                                      $<$<CONFIG:Release>:${CONAN_COMPILE_DEFINITIONS_EIGEN3_RELEASE}>
                                                                      $<$<CONFIG:RelWithDebInfo>:${CONAN_COMPILE_DEFINITIONS_EIGEN3_RELWITHDEBINFO}>
                                                                      $<$<CONFIG:MinSizeRel>:${CONAN_COMPILE_DEFINITIONS_EIGEN3_MINSIZEREL}>
                                                                      $<$<CONFIG:Debug>:${CONAN_COMPILE_DEFINITIONS_EIGEN3_DEBUG}>)
    set_property(TARGET CONAN_PKG::Eigen3 PROPERTY INTERFACE_COMPILE_OPTIONS ${CONAN_C_FLAGS_EIGEN3_LIST} ${CONAN_CXX_FLAGS_EIGEN3_LIST}
                                                                  $<$<CONFIG:Release>:${CONAN_C_FLAGS_EIGEN3_RELEASE_LIST} ${CONAN_CXX_FLAGS_EIGEN3_RELEASE_LIST}>
                                                                  $<$<CONFIG:RelWithDebInfo>:${CONAN_C_FLAGS_EIGEN3_RELWITHDEBINFO_LIST} ${CONAN_CXX_FLAGS_EIGEN3_RELWITHDEBINFO_LIST}>
                                                                  $<$<CONFIG:MinSizeRel>:${CONAN_C_FLAGS_EIGEN3_MINSIZEREL_LIST} ${CONAN_CXX_FLAGS_EIGEN3_MINSIZEREL_LIST}>
                                                                  $<$<CONFIG:Debug>:${CONAN_C_FLAGS_EIGEN3_DEBUG_LIST}  ${CONAN_CXX_FLAGS_EIGEN3_DEBUG_LIST}>)


    set(_CONAN_PKG_LIBS_FMT_DEPENDENCIES "${CONAN_SYSTEM_LIBS_FMT} ${CONAN_FRAMEWORKS_FOUND_FMT} ")
    string(REPLACE " " ";" _CONAN_PKG_LIBS_FMT_DEPENDENCIES "${_CONAN_PKG_LIBS_FMT_DEPENDENCIES}")
    conan_package_library_targets("${CONAN_PKG_LIBS_FMT}" "${CONAN_LIB_DIRS_FMT}"
                                  CONAN_PACKAGE_TARGETS_FMT "${_CONAN_PKG_LIBS_FMT_DEPENDENCIES}"
                                  "" fmt)
    set(_CONAN_PKG_LIBS_FMT_DEPENDENCIES_DEBUG "${CONAN_SYSTEM_LIBS_FMT_DEBUG} ${CONAN_FRAMEWORKS_FOUND_FMT_DEBUG} ")
    string(REPLACE " " ";" _CONAN_PKG_LIBS_FMT_DEPENDENCIES_DEBUG "${_CONAN_PKG_LIBS_FMT_DEPENDENCIES_DEBUG}")
    conan_package_library_targets("${CONAN_PKG_LIBS_FMT_DEBUG}" "${CONAN_LIB_DIRS_FMT_DEBUG}"
                                  CONAN_PACKAGE_TARGETS_FMT_DEBUG "${_CONAN_PKG_LIBS_FMT_DEPENDENCIES_DEBUG}"
                                  "debug" fmt)
    set(_CONAN_PKG_LIBS_FMT_DEPENDENCIES_RELEASE "${CONAN_SYSTEM_LIBS_FMT_RELEASE} ${CONAN_FRAMEWORKS_FOUND_FMT_RELEASE} ")
    string(REPLACE " " ";" _CONAN_PKG_LIBS_FMT_DEPENDENCIES_RELEASE "${_CONAN_PKG_LIBS_FMT_DEPENDENCIES_RELEASE}")
    conan_package_library_targets("${CONAN_PKG_LIBS_FMT_RELEASE}" "${CONAN_LIB_DIRS_FMT_RELEASE}"
                                  CONAN_PACKAGE_TARGETS_FMT_RELEASE "${_CONAN_PKG_LIBS_FMT_DEPENDENCIES_RELEASE}"
                                  "release" fmt)
    set(_CONAN_PKG_LIBS_FMT_DEPENDENCIES_RELWITHDEBINFO "${CONAN_SYSTEM_LIBS_FMT_RELWITHDEBINFO} ${CONAN_FRAMEWORKS_FOUND_FMT_RELWITHDEBINFO} ")
    string(REPLACE " " ";" _CONAN_PKG_LIBS_FMT_DEPENDENCIES_RELWITHDEBINFO "${_CONAN_PKG_LIBS_FMT_DEPENDENCIES_RELWITHDEBINFO}")
    conan_package_library_targets("${CONAN_PKG_LIBS_FMT_RELWITHDEBINFO}" "${CONAN_LIB_DIRS_FMT_RELWITHDEBINFO}"
                                  CONAN_PACKAGE_TARGETS_FMT_RELWITHDEBINFO "${_CONAN_PKG_LIBS_FMT_DEPENDENCIES_RELWITHDEBINFO}"
                                  "relwithdebinfo" fmt)
    set(_CONAN_PKG_LIBS_FMT_DEPENDENCIES_MINSIZEREL "${CONAN_SYSTEM_LIBS_FMT_MINSIZEREL} ${CONAN_FRAMEWORKS_FOUND_FMT_MINSIZEREL} ")
    string(REPLACE " " ";" _CONAN_PKG_LIBS_FMT_DEPENDENCIES_MINSIZEREL "${_CONAN_PKG_LIBS_FMT_DEPENDENCIES_MINSIZEREL}")
    conan_package_library_targets("${CONAN_PKG_LIBS_FMT_MINSIZEREL}" "${CONAN_LIB_DIRS_FMT_MINSIZEREL}"
                                  CONAN_PACKAGE_TARGETS_FMT_MINSIZEREL "${_CONAN_PKG_LIBS_FMT_DEPENDENCIES_MINSIZEREL}"
                                  "minsizerel" fmt)

    add_library(CONAN_PKG::fmt INTERFACE IMPORTED)

    # Property INTERFACE_LINK_FLAGS do not work, necessary to add to INTERFACE_LINK_LIBRARIES
    set_property(TARGET CONAN_PKG::fmt PROPERTY INTERFACE_LINK_LIBRARIES ${CONAN_PACKAGE_TARGETS_FMT} ${_CONAN_PKG_LIBS_FMT_DEPENDENCIES}
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_FMT_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_FMT_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:${CONAN_EXE_LINKER_FLAGS_FMT_LIST}>

                                                                 $<$<CONFIG:Release>:${CONAN_PACKAGE_TARGETS_FMT_RELEASE} ${_CONAN_PKG_LIBS_FMT_DEPENDENCIES_RELEASE}
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_FMT_RELEASE_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_FMT_RELEASE_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:${CONAN_EXE_LINKER_FLAGS_FMT_RELEASE_LIST}>>

                                                                 $<$<CONFIG:RelWithDebInfo>:${CONAN_PACKAGE_TARGETS_FMT_RELWITHDEBINFO} ${_CONAN_PKG_LIBS_FMT_DEPENDENCIES_RELWITHDEBINFO}
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_FMT_RELWITHDEBINFO_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_FMT_RELWITHDEBINFO_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:${CONAN_EXE_LINKER_FLAGS_FMT_RELWITHDEBINFO_LIST}>>

                                                                 $<$<CONFIG:MinSizeRel>:${CONAN_PACKAGE_TARGETS_FMT_MINSIZEREL} ${_CONAN_PKG_LIBS_FMT_DEPENDENCIES_MINSIZEREL}
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_FMT_MINSIZEREL_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_FMT_MINSIZEREL_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:${CONAN_EXE_LINKER_FLAGS_FMT_MINSIZEREL_LIST}>>

                                                                 $<$<CONFIG:Debug>:${CONAN_PACKAGE_TARGETS_FMT_DEBUG} ${_CONAN_PKG_LIBS_FMT_DEPENDENCIES_DEBUG}
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_FMT_DEBUG_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_FMT_DEBUG_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:${CONAN_EXE_LINKER_FLAGS_FMT_DEBUG_LIST}>>)
    set_property(TARGET CONAN_PKG::fmt PROPERTY INTERFACE_INCLUDE_DIRECTORIES ${CONAN_INCLUDE_DIRS_FMT}
                                                                      $<$<CONFIG:Release>:${CONAN_INCLUDE_DIRS_FMT_RELEASE}>
                                                                      $<$<CONFIG:RelWithDebInfo>:${CONAN_INCLUDE_DIRS_FMT_RELWITHDEBINFO}>
                                                                      $<$<CONFIG:MinSizeRel>:${CONAN_INCLUDE_DIRS_FMT_MINSIZEREL}>
                                                                      $<$<CONFIG:Debug>:${CONAN_INCLUDE_DIRS_FMT_DEBUG}>)
    set_property(TARGET CONAN_PKG::fmt PROPERTY INTERFACE_COMPILE_DEFINITIONS ${CONAN_COMPILE_DEFINITIONS_FMT}
                                                                      $<$<CONFIG:Release>:${CONAN_COMPILE_DEFINITIONS_FMT_RELEASE}>
                                                                      $<$<CONFIG:RelWithDebInfo>:${CONAN_COMPILE_DEFINITIONS_FMT_RELWITHDEBINFO}>
                                                                      $<$<CONFIG:MinSizeRel>:${CONAN_COMPILE_DEFINITIONS_FMT_MINSIZEREL}>
                                                                      $<$<CONFIG:Debug>:${CONAN_COMPILE_DEFINITIONS_FMT_DEBUG}>)
    set_property(TARGET CONAN_PKG::fmt PROPERTY INTERFACE_COMPILE_OPTIONS ${CONAN_C_FLAGS_FMT_LIST} ${CONAN_CXX_FLAGS_FMT_LIST}
                                                                  $<$<CONFIG:Release>:${CONAN_C_FLAGS_FMT_RELEASE_LIST} ${CONAN_CXX_FLAGS_FMT_RELEASE_LIST}>
                                                                  $<$<CONFIG:RelWithDebInfo>:${CONAN_C_FLAGS_FMT_RELWITHDEBINFO_LIST} ${CONAN_CXX_FLAGS_FMT_RELWITHDEBINFO_LIST}>
                                                                  $<$<CONFIG:MinSizeRel>:${CONAN_C_FLAGS_FMT_MINSIZEREL_LIST} ${CONAN_CXX_FLAGS_FMT_MINSIZEREL_LIST}>
                                                                  $<$<CONFIG:Debug>:${CONAN_C_FLAGS_FMT_DEBUG_LIST}  ${CONAN_CXX_FLAGS_FMT_DEBUG_LIST}>)


    set(_CONAN_PKG_LIBS_OSQP_DEPENDENCIES "${CONAN_SYSTEM_LIBS_OSQP} ${CONAN_FRAMEWORKS_FOUND_OSQP} ")
    string(REPLACE " " ";" _CONAN_PKG_LIBS_OSQP_DEPENDENCIES "${_CONAN_PKG_LIBS_OSQP_DEPENDENCIES}")
    conan_package_library_targets("${CONAN_PKG_LIBS_OSQP}" "${CONAN_LIB_DIRS_OSQP}"
                                  CONAN_PACKAGE_TARGETS_OSQP "${_CONAN_PKG_LIBS_OSQP_DEPENDENCIES}"
                                  "" osqp)
    set(_CONAN_PKG_LIBS_OSQP_DEPENDENCIES_DEBUG "${CONAN_SYSTEM_LIBS_OSQP_DEBUG} ${CONAN_FRAMEWORKS_FOUND_OSQP_DEBUG} ")
    string(REPLACE " " ";" _CONAN_PKG_LIBS_OSQP_DEPENDENCIES_DEBUG "${_CONAN_PKG_LIBS_OSQP_DEPENDENCIES_DEBUG}")
    conan_package_library_targets("${CONAN_PKG_LIBS_OSQP_DEBUG}" "${CONAN_LIB_DIRS_OSQP_DEBUG}"
                                  CONAN_PACKAGE_TARGETS_OSQP_DEBUG "${_CONAN_PKG_LIBS_OSQP_DEPENDENCIES_DEBUG}"
                                  "debug" osqp)
    set(_CONAN_PKG_LIBS_OSQP_DEPENDENCIES_RELEASE "${CONAN_SYSTEM_LIBS_OSQP_RELEASE} ${CONAN_FRAMEWORKS_FOUND_OSQP_RELEASE} ")
    string(REPLACE " " ";" _CONAN_PKG_LIBS_OSQP_DEPENDENCIES_RELEASE "${_CONAN_PKG_LIBS_OSQP_DEPENDENCIES_RELEASE}")
    conan_package_library_targets("${CONAN_PKG_LIBS_OSQP_RELEASE}" "${CONAN_LIB_DIRS_OSQP_RELEASE}"
                                  CONAN_PACKAGE_TARGETS_OSQP_RELEASE "${_CONAN_PKG_LIBS_OSQP_DEPENDENCIES_RELEASE}"
                                  "release" osqp)
    set(_CONAN_PKG_LIBS_OSQP_DEPENDENCIES_RELWITHDEBINFO "${CONAN_SYSTEM_LIBS_OSQP_RELWITHDEBINFO} ${CONAN_FRAMEWORKS_FOUND_OSQP_RELWITHDEBINFO} ")
    string(REPLACE " " ";" _CONAN_PKG_LIBS_OSQP_DEPENDENCIES_RELWITHDEBINFO "${_CONAN_PKG_LIBS_OSQP_DEPENDENCIES_RELWITHDEBINFO}")
    conan_package_library_targets("${CONAN_PKG_LIBS_OSQP_RELWITHDEBINFO}" "${CONAN_LIB_DIRS_OSQP_RELWITHDEBINFO}"
                                  CONAN_PACKAGE_TARGETS_OSQP_RELWITHDEBINFO "${_CONAN_PKG_LIBS_OSQP_DEPENDENCIES_RELWITHDEBINFO}"
                                  "relwithdebinfo" osqp)
    set(_CONAN_PKG_LIBS_OSQP_DEPENDENCIES_MINSIZEREL "${CONAN_SYSTEM_LIBS_OSQP_MINSIZEREL} ${CONAN_FRAMEWORKS_FOUND_OSQP_MINSIZEREL} ")
    string(REPLACE " " ";" _CONAN_PKG_LIBS_OSQP_DEPENDENCIES_MINSIZEREL "${_CONAN_PKG_LIBS_OSQP_DEPENDENCIES_MINSIZEREL}")
    conan_package_library_targets("${CONAN_PKG_LIBS_OSQP_MINSIZEREL}" "${CONAN_LIB_DIRS_OSQP_MINSIZEREL}"
                                  CONAN_PACKAGE_TARGETS_OSQP_MINSIZEREL "${_CONAN_PKG_LIBS_OSQP_DEPENDENCIES_MINSIZEREL}"
                                  "minsizerel" osqp)

    add_library(CONAN_PKG::osqp INTERFACE IMPORTED)

    # Property INTERFACE_LINK_FLAGS do not work, necessary to add to INTERFACE_LINK_LIBRARIES
    set_property(TARGET CONAN_PKG::osqp PROPERTY INTERFACE_LINK_LIBRARIES ${CONAN_PACKAGE_TARGETS_OSQP} ${_CONAN_PKG_LIBS_OSQP_DEPENDENCIES}
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_OSQP_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_OSQP_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:${CONAN_EXE_LINKER_FLAGS_OSQP_LIST}>

                                                                 $<$<CONFIG:Release>:${CONAN_PACKAGE_TARGETS_OSQP_RELEASE} ${_CONAN_PKG_LIBS_OSQP_DEPENDENCIES_RELEASE}
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_OSQP_RELEASE_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_OSQP_RELEASE_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:${CONAN_EXE_LINKER_FLAGS_OSQP_RELEASE_LIST}>>

                                                                 $<$<CONFIG:RelWithDebInfo>:${CONAN_PACKAGE_TARGETS_OSQP_RELWITHDEBINFO} ${_CONAN_PKG_LIBS_OSQP_DEPENDENCIES_RELWITHDEBINFO}
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_OSQP_RELWITHDEBINFO_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_OSQP_RELWITHDEBINFO_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:${CONAN_EXE_LINKER_FLAGS_OSQP_RELWITHDEBINFO_LIST}>>

                                                                 $<$<CONFIG:MinSizeRel>:${CONAN_PACKAGE_TARGETS_OSQP_MINSIZEREL} ${_CONAN_PKG_LIBS_OSQP_DEPENDENCIES_MINSIZEREL}
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_OSQP_MINSIZEREL_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_OSQP_MINSIZEREL_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:${CONAN_EXE_LINKER_FLAGS_OSQP_MINSIZEREL_LIST}>>

                                                                 $<$<CONFIG:Debug>:${CONAN_PACKAGE_TARGETS_OSQP_DEBUG} ${_CONAN_PKG_LIBS_OSQP_DEPENDENCIES_DEBUG}
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_OSQP_DEBUG_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_OSQP_DEBUG_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:${CONAN_EXE_LINKER_FLAGS_OSQP_DEBUG_LIST}>>)
    set_property(TARGET CONAN_PKG::osqp PROPERTY INTERFACE_INCLUDE_DIRECTORIES ${CONAN_INCLUDE_DIRS_OSQP}
                                                                      $<$<CONFIG:Release>:${CONAN_INCLUDE_DIRS_OSQP_RELEASE}>
                                                                      $<$<CONFIG:RelWithDebInfo>:${CONAN_INCLUDE_DIRS_OSQP_RELWITHDEBINFO}>
                                                                      $<$<CONFIG:MinSizeRel>:${CONAN_INCLUDE_DIRS_OSQP_MINSIZEREL}>
                                                                      $<$<CONFIG:Debug>:${CONAN_INCLUDE_DIRS_OSQP_DEBUG}>)
    set_property(TARGET CONAN_PKG::osqp PROPERTY INTERFACE_COMPILE_DEFINITIONS ${CONAN_COMPILE_DEFINITIONS_OSQP}
                                                                      $<$<CONFIG:Release>:${CONAN_COMPILE_DEFINITIONS_OSQP_RELEASE}>
                                                                      $<$<CONFIG:RelWithDebInfo>:${CONAN_COMPILE_DEFINITIONS_OSQP_RELWITHDEBINFO}>
                                                                      $<$<CONFIG:MinSizeRel>:${CONAN_COMPILE_DEFINITIONS_OSQP_MINSIZEREL}>
                                                                      $<$<CONFIG:Debug>:${CONAN_COMPILE_DEFINITIONS_OSQP_DEBUG}>)
    set_property(TARGET CONAN_PKG::osqp PROPERTY INTERFACE_COMPILE_OPTIONS ${CONAN_C_FLAGS_OSQP_LIST} ${CONAN_CXX_FLAGS_OSQP_LIST}
                                                                  $<$<CONFIG:Release>:${CONAN_C_FLAGS_OSQP_RELEASE_LIST} ${CONAN_CXX_FLAGS_OSQP_RELEASE_LIST}>
                                                                  $<$<CONFIG:RelWithDebInfo>:${CONAN_C_FLAGS_OSQP_RELWITHDEBINFO_LIST} ${CONAN_CXX_FLAGS_OSQP_RELWITHDEBINFO_LIST}>
                                                                  $<$<CONFIG:MinSizeRel>:${CONAN_C_FLAGS_OSQP_MINSIZEREL_LIST} ${CONAN_CXX_FLAGS_OSQP_MINSIZEREL_LIST}>
                                                                  $<$<CONFIG:Debug>:${CONAN_C_FLAGS_OSQP_DEBUG_LIST}  ${CONAN_CXX_FLAGS_OSQP_DEBUG_LIST}>)


    set(_CONAN_PKG_LIBS_ECOS_DEPENDENCIES "${CONAN_SYSTEM_LIBS_ECOS} ${CONAN_FRAMEWORKS_FOUND_ECOS} ")
    string(REPLACE " " ";" _CONAN_PKG_LIBS_ECOS_DEPENDENCIES "${_CONAN_PKG_LIBS_ECOS_DEPENDENCIES}")
    conan_package_library_targets("${CONAN_PKG_LIBS_ECOS}" "${CONAN_LIB_DIRS_ECOS}"
                                  CONAN_PACKAGE_TARGETS_ECOS "${_CONAN_PKG_LIBS_ECOS_DEPENDENCIES}"
                                  "" ecos)
    set(_CONAN_PKG_LIBS_ECOS_DEPENDENCIES_DEBUG "${CONAN_SYSTEM_LIBS_ECOS_DEBUG} ${CONAN_FRAMEWORKS_FOUND_ECOS_DEBUG} ")
    string(REPLACE " " ";" _CONAN_PKG_LIBS_ECOS_DEPENDENCIES_DEBUG "${_CONAN_PKG_LIBS_ECOS_DEPENDENCIES_DEBUG}")
    conan_package_library_targets("${CONAN_PKG_LIBS_ECOS_DEBUG}" "${CONAN_LIB_DIRS_ECOS_DEBUG}"
                                  CONAN_PACKAGE_TARGETS_ECOS_DEBUG "${_CONAN_PKG_LIBS_ECOS_DEPENDENCIES_DEBUG}"
                                  "debug" ecos)
    set(_CONAN_PKG_LIBS_ECOS_DEPENDENCIES_RELEASE "${CONAN_SYSTEM_LIBS_ECOS_RELEASE} ${CONAN_FRAMEWORKS_FOUND_ECOS_RELEASE} ")
    string(REPLACE " " ";" _CONAN_PKG_LIBS_ECOS_DEPENDENCIES_RELEASE "${_CONAN_PKG_LIBS_ECOS_DEPENDENCIES_RELEASE}")
    conan_package_library_targets("${CONAN_PKG_LIBS_ECOS_RELEASE}" "${CONAN_LIB_DIRS_ECOS_RELEASE}"
                                  CONAN_PACKAGE_TARGETS_ECOS_RELEASE "${_CONAN_PKG_LIBS_ECOS_DEPENDENCIES_RELEASE}"
                                  "release" ecos)
    set(_CONAN_PKG_LIBS_ECOS_DEPENDENCIES_RELWITHDEBINFO "${CONAN_SYSTEM_LIBS_ECOS_RELWITHDEBINFO} ${CONAN_FRAMEWORKS_FOUND_ECOS_RELWITHDEBINFO} ")
    string(REPLACE " " ";" _CONAN_PKG_LIBS_ECOS_DEPENDENCIES_RELWITHDEBINFO "${_CONAN_PKG_LIBS_ECOS_DEPENDENCIES_RELWITHDEBINFO}")
    conan_package_library_targets("${CONAN_PKG_LIBS_ECOS_RELWITHDEBINFO}" "${CONAN_LIB_DIRS_ECOS_RELWITHDEBINFO}"
                                  CONAN_PACKAGE_TARGETS_ECOS_RELWITHDEBINFO "${_CONAN_PKG_LIBS_ECOS_DEPENDENCIES_RELWITHDEBINFO}"
                                  "relwithdebinfo" ecos)
    set(_CONAN_PKG_LIBS_ECOS_DEPENDENCIES_MINSIZEREL "${CONAN_SYSTEM_LIBS_ECOS_MINSIZEREL} ${CONAN_FRAMEWORKS_FOUND_ECOS_MINSIZEREL} ")
    string(REPLACE " " ";" _CONAN_PKG_LIBS_ECOS_DEPENDENCIES_MINSIZEREL "${_CONAN_PKG_LIBS_ECOS_DEPENDENCIES_MINSIZEREL}")
    conan_package_library_targets("${CONAN_PKG_LIBS_ECOS_MINSIZEREL}" "${CONAN_LIB_DIRS_ECOS_MINSIZEREL}"
                                  CONAN_PACKAGE_TARGETS_ECOS_MINSIZEREL "${_CONAN_PKG_LIBS_ECOS_DEPENDENCIES_MINSIZEREL}"
                                  "minsizerel" ecos)

    add_library(CONAN_PKG::ecos INTERFACE IMPORTED)

    # Property INTERFACE_LINK_FLAGS do not work, necessary to add to INTERFACE_LINK_LIBRARIES
    set_property(TARGET CONAN_PKG::ecos PROPERTY INTERFACE_LINK_LIBRARIES ${CONAN_PACKAGE_TARGETS_ECOS} ${_CONAN_PKG_LIBS_ECOS_DEPENDENCIES}
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_ECOS_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_ECOS_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:${CONAN_EXE_LINKER_FLAGS_ECOS_LIST}>

                                                                 $<$<CONFIG:Release>:${CONAN_PACKAGE_TARGETS_ECOS_RELEASE} ${_CONAN_PKG_LIBS_ECOS_DEPENDENCIES_RELEASE}
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_ECOS_RELEASE_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_ECOS_RELEASE_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:${CONAN_EXE_LINKER_FLAGS_ECOS_RELEASE_LIST}>>

                                                                 $<$<CONFIG:RelWithDebInfo>:${CONAN_PACKAGE_TARGETS_ECOS_RELWITHDEBINFO} ${_CONAN_PKG_LIBS_ECOS_DEPENDENCIES_RELWITHDEBINFO}
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_ECOS_RELWITHDEBINFO_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_ECOS_RELWITHDEBINFO_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:${CONAN_EXE_LINKER_FLAGS_ECOS_RELWITHDEBINFO_LIST}>>

                                                                 $<$<CONFIG:MinSizeRel>:${CONAN_PACKAGE_TARGETS_ECOS_MINSIZEREL} ${_CONAN_PKG_LIBS_ECOS_DEPENDENCIES_MINSIZEREL}
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_ECOS_MINSIZEREL_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_ECOS_MINSIZEREL_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:${CONAN_EXE_LINKER_FLAGS_ECOS_MINSIZEREL_LIST}>>

                                                                 $<$<CONFIG:Debug>:${CONAN_PACKAGE_TARGETS_ECOS_DEBUG} ${_CONAN_PKG_LIBS_ECOS_DEPENDENCIES_DEBUG}
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_ECOS_DEBUG_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_ECOS_DEBUG_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:${CONAN_EXE_LINKER_FLAGS_ECOS_DEBUG_LIST}>>)
    set_property(TARGET CONAN_PKG::ecos PROPERTY INTERFACE_INCLUDE_DIRECTORIES ${CONAN_INCLUDE_DIRS_ECOS}
                                                                      $<$<CONFIG:Release>:${CONAN_INCLUDE_DIRS_ECOS_RELEASE}>
                                                                      $<$<CONFIG:RelWithDebInfo>:${CONAN_INCLUDE_DIRS_ECOS_RELWITHDEBINFO}>
                                                                      $<$<CONFIG:MinSizeRel>:${CONAN_INCLUDE_DIRS_ECOS_MINSIZEREL}>
                                                                      $<$<CONFIG:Debug>:${CONAN_INCLUDE_DIRS_ECOS_DEBUG}>)
    set_property(TARGET CONAN_PKG::ecos PROPERTY INTERFACE_COMPILE_DEFINITIONS ${CONAN_COMPILE_DEFINITIONS_ECOS}
                                                                      $<$<CONFIG:Release>:${CONAN_COMPILE_DEFINITIONS_ECOS_RELEASE}>
                                                                      $<$<CONFIG:RelWithDebInfo>:${CONAN_COMPILE_DEFINITIONS_ECOS_RELWITHDEBINFO}>
                                                                      $<$<CONFIG:MinSizeRel>:${CONAN_COMPILE_DEFINITIONS_ECOS_MINSIZEREL}>
                                                                      $<$<CONFIG:Debug>:${CONAN_COMPILE_DEFINITIONS_ECOS_DEBUG}>)
    set_property(TARGET CONAN_PKG::ecos PROPERTY INTERFACE_COMPILE_OPTIONS ${CONAN_C_FLAGS_ECOS_LIST} ${CONAN_CXX_FLAGS_ECOS_LIST}
                                                                  $<$<CONFIG:Release>:${CONAN_C_FLAGS_ECOS_RELEASE_LIST} ${CONAN_CXX_FLAGS_ECOS_RELEASE_LIST}>
                                                                  $<$<CONFIG:RelWithDebInfo>:${CONAN_C_FLAGS_ECOS_RELWITHDEBINFO_LIST} ${CONAN_CXX_FLAGS_ECOS_RELWITHDEBINFO_LIST}>
                                                                  $<$<CONFIG:MinSizeRel>:${CONAN_C_FLAGS_ECOS_MINSIZEREL_LIST} ${CONAN_CXX_FLAGS_ECOS_MINSIZEREL_LIST}>
                                                                  $<$<CONFIG:Debug>:${CONAN_C_FLAGS_ECOS_DEBUG_LIST}  ${CONAN_CXX_FLAGS_ECOS_DEBUG_LIST}>)


    set(_CONAN_PKG_LIBS_TINYXML2_DEPENDENCIES "${CONAN_SYSTEM_LIBS_TINYXML2} ${CONAN_FRAMEWORKS_FOUND_TINYXML2} ")
    string(REPLACE " " ";" _CONAN_PKG_LIBS_TINYXML2_DEPENDENCIES "${_CONAN_PKG_LIBS_TINYXML2_DEPENDENCIES}")
    conan_package_library_targets("${CONAN_PKG_LIBS_TINYXML2}" "${CONAN_LIB_DIRS_TINYXML2}"
                                  CONAN_PACKAGE_TARGETS_TINYXML2 "${_CONAN_PKG_LIBS_TINYXML2_DEPENDENCIES}"
                                  "" tinyxml2)
    set(_CONAN_PKG_LIBS_TINYXML2_DEPENDENCIES_DEBUG "${CONAN_SYSTEM_LIBS_TINYXML2_DEBUG} ${CONAN_FRAMEWORKS_FOUND_TINYXML2_DEBUG} ")
    string(REPLACE " " ";" _CONAN_PKG_LIBS_TINYXML2_DEPENDENCIES_DEBUG "${_CONAN_PKG_LIBS_TINYXML2_DEPENDENCIES_DEBUG}")
    conan_package_library_targets("${CONAN_PKG_LIBS_TINYXML2_DEBUG}" "${CONAN_LIB_DIRS_TINYXML2_DEBUG}"
                                  CONAN_PACKAGE_TARGETS_TINYXML2_DEBUG "${_CONAN_PKG_LIBS_TINYXML2_DEPENDENCIES_DEBUG}"
                                  "debug" tinyxml2)
    set(_CONAN_PKG_LIBS_TINYXML2_DEPENDENCIES_RELEASE "${CONAN_SYSTEM_LIBS_TINYXML2_RELEASE} ${CONAN_FRAMEWORKS_FOUND_TINYXML2_RELEASE} ")
    string(REPLACE " " ";" _CONAN_PKG_LIBS_TINYXML2_DEPENDENCIES_RELEASE "${_CONAN_PKG_LIBS_TINYXML2_DEPENDENCIES_RELEASE}")
    conan_package_library_targets("${CONAN_PKG_LIBS_TINYXML2_RELEASE}" "${CONAN_LIB_DIRS_TINYXML2_RELEASE}"
                                  CONAN_PACKAGE_TARGETS_TINYXML2_RELEASE "${_CONAN_PKG_LIBS_TINYXML2_DEPENDENCIES_RELEASE}"
                                  "release" tinyxml2)
    set(_CONAN_PKG_LIBS_TINYXML2_DEPENDENCIES_RELWITHDEBINFO "${CONAN_SYSTEM_LIBS_TINYXML2_RELWITHDEBINFO} ${CONAN_FRAMEWORKS_FOUND_TINYXML2_RELWITHDEBINFO} ")
    string(REPLACE " " ";" _CONAN_PKG_LIBS_TINYXML2_DEPENDENCIES_RELWITHDEBINFO "${_CONAN_PKG_LIBS_TINYXML2_DEPENDENCIES_RELWITHDEBINFO}")
    conan_package_library_targets("${CONAN_PKG_LIBS_TINYXML2_RELWITHDEBINFO}" "${CONAN_LIB_DIRS_TINYXML2_RELWITHDEBINFO}"
                                  CONAN_PACKAGE_TARGETS_TINYXML2_RELWITHDEBINFO "${_CONAN_PKG_LIBS_TINYXML2_DEPENDENCIES_RELWITHDEBINFO}"
                                  "relwithdebinfo" tinyxml2)
    set(_CONAN_PKG_LIBS_TINYXML2_DEPENDENCIES_MINSIZEREL "${CONAN_SYSTEM_LIBS_TINYXML2_MINSIZEREL} ${CONAN_FRAMEWORKS_FOUND_TINYXML2_MINSIZEREL} ")
    string(REPLACE " " ";" _CONAN_PKG_LIBS_TINYXML2_DEPENDENCIES_MINSIZEREL "${_CONAN_PKG_LIBS_TINYXML2_DEPENDENCIES_MINSIZEREL}")
    conan_package_library_targets("${CONAN_PKG_LIBS_TINYXML2_MINSIZEREL}" "${CONAN_LIB_DIRS_TINYXML2_MINSIZEREL}"
                                  CONAN_PACKAGE_TARGETS_TINYXML2_MINSIZEREL "${_CONAN_PKG_LIBS_TINYXML2_DEPENDENCIES_MINSIZEREL}"
                                  "minsizerel" tinyxml2)

    add_library(CONAN_PKG::tinyxml2 INTERFACE IMPORTED)

    # Property INTERFACE_LINK_FLAGS do not work, necessary to add to INTERFACE_LINK_LIBRARIES
    set_property(TARGET CONAN_PKG::tinyxml2 PROPERTY INTERFACE_LINK_LIBRARIES ${CONAN_PACKAGE_TARGETS_TINYXML2} ${_CONAN_PKG_LIBS_TINYXML2_DEPENDENCIES}
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_TINYXML2_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_TINYXML2_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:${CONAN_EXE_LINKER_FLAGS_TINYXML2_LIST}>

                                                                 $<$<CONFIG:Release>:${CONAN_PACKAGE_TARGETS_TINYXML2_RELEASE} ${_CONAN_PKG_LIBS_TINYXML2_DEPENDENCIES_RELEASE}
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_TINYXML2_RELEASE_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_TINYXML2_RELEASE_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:${CONAN_EXE_LINKER_FLAGS_TINYXML2_RELEASE_LIST}>>

                                                                 $<$<CONFIG:RelWithDebInfo>:${CONAN_PACKAGE_TARGETS_TINYXML2_RELWITHDEBINFO} ${_CONAN_PKG_LIBS_TINYXML2_DEPENDENCIES_RELWITHDEBINFO}
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_TINYXML2_RELWITHDEBINFO_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_TINYXML2_RELWITHDEBINFO_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:${CONAN_EXE_LINKER_FLAGS_TINYXML2_RELWITHDEBINFO_LIST}>>

                                                                 $<$<CONFIG:MinSizeRel>:${CONAN_PACKAGE_TARGETS_TINYXML2_MINSIZEREL} ${_CONAN_PKG_LIBS_TINYXML2_DEPENDENCIES_MINSIZEREL}
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_TINYXML2_MINSIZEREL_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_TINYXML2_MINSIZEREL_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:${CONAN_EXE_LINKER_FLAGS_TINYXML2_MINSIZEREL_LIST}>>

                                                                 $<$<CONFIG:Debug>:${CONAN_PACKAGE_TARGETS_TINYXML2_DEBUG} ${_CONAN_PKG_LIBS_TINYXML2_DEPENDENCIES_DEBUG}
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_TINYXML2_DEBUG_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:${CONAN_SHARED_LINKER_FLAGS_TINYXML2_DEBUG_LIST}>
                                                                 $<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:${CONAN_EXE_LINKER_FLAGS_TINYXML2_DEBUG_LIST}>>)
    set_property(TARGET CONAN_PKG::tinyxml2 PROPERTY INTERFACE_INCLUDE_DIRECTORIES ${CONAN_INCLUDE_DIRS_TINYXML2}
                                                                      $<$<CONFIG:Release>:${CONAN_INCLUDE_DIRS_TINYXML2_RELEASE}>
                                                                      $<$<CONFIG:RelWithDebInfo>:${CONAN_INCLUDE_DIRS_TINYXML2_RELWITHDEBINFO}>
                                                                      $<$<CONFIG:MinSizeRel>:${CONAN_INCLUDE_DIRS_TINYXML2_MINSIZEREL}>
                                                                      $<$<CONFIG:Debug>:${CONAN_INCLUDE_DIRS_TINYXML2_DEBUG}>)
    set_property(TARGET CONAN_PKG::tinyxml2 PROPERTY INTERFACE_COMPILE_DEFINITIONS ${CONAN_COMPILE_DEFINITIONS_TINYXML2}
                                                                      $<$<CONFIG:Release>:${CONAN_COMPILE_DEFINITIONS_TINYXML2_RELEASE}>
                                                                      $<$<CONFIG:RelWithDebInfo>:${CONAN_COMPILE_DEFINITIONS_TINYXML2_RELWITHDEBINFO}>
                                                                      $<$<CONFIG:MinSizeRel>:${CONAN_COMPILE_DEFINITIONS_TINYXML2_MINSIZEREL}>
                                                                      $<$<CONFIG:Debug>:${CONAN_COMPILE_DEFINITIONS_TINYXML2_DEBUG}>)
    set_property(TARGET CONAN_PKG::tinyxml2 PROPERTY INTERFACE_COMPILE_OPTIONS ${CONAN_C_FLAGS_TINYXML2_LIST} ${CONAN_CXX_FLAGS_TINYXML2_LIST}
                                                                  $<$<CONFIG:Release>:${CONAN_C_FLAGS_TINYXML2_RELEASE_LIST} ${CONAN_CXX_FLAGS_TINYXML2_RELEASE_LIST}>
                                                                  $<$<CONFIG:RelWithDebInfo>:${CONAN_C_FLAGS_TINYXML2_RELWITHDEBINFO_LIST} ${CONAN_CXX_FLAGS_TINYXML2_RELWITHDEBINFO_LIST}>
                                                                  $<$<CONFIG:MinSizeRel>:${CONAN_C_FLAGS_TINYXML2_MINSIZEREL_LIST} ${CONAN_CXX_FLAGS_TINYXML2_MINSIZEREL_LIST}>
                                                                  $<$<CONFIG:Debug>:${CONAN_C_FLAGS_TINYXML2_DEBUG_LIST}  ${CONAN_CXX_FLAGS_TINYXML2_DEBUG_LIST}>)

    set(CONAN_TARGETS CONAN_PKG::robot-trajectory CONAN_PKG::robot-control CONAN_PKG::robot-model CONAN_PKG::eigen-extensions CONAN_PKG::epigraph CONAN_PKG::urdfdomcpp CONAN_PKG::Eigen3 CONAN_PKG::fmt CONAN_PKG::osqp CONAN_PKG::ecos CONAN_PKG::tinyxml2)

endmacro()


macro(conan_basic_setup)
    set(options TARGETS NO_OUTPUT_DIRS SKIP_RPATH KEEP_RPATHS SKIP_STD SKIP_FPIC)
    cmake_parse_arguments(ARGUMENTS "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

    if(CONAN_EXPORTED)
        conan_message(STATUS "Conan: called by CMake conan helper")
    endif()

    if(CONAN_IN_LOCAL_CACHE)
        conan_message(STATUS "Conan: called inside local cache")
    endif()

    if(NOT ARGUMENTS_NO_OUTPUT_DIRS)
        conan_message(STATUS "Conan: Adjusting output directories")
        conan_output_dirs_setup()
    endif()

    if(NOT ARGUMENTS_TARGETS)
        conan_message(STATUS "Conan: Using cmake global configuration")
        conan_global_flags()
    else()
        conan_message(STATUS "Conan: Using cmake targets configuration")
        conan_define_targets()
    endif()

    if(ARGUMENTS_SKIP_RPATH)
        # Change by "DEPRECATION" or "SEND_ERROR" when we are ready
        conan_message(WARNING "Conan: SKIP_RPATH is deprecated, it has been renamed to KEEP_RPATHS")
    endif()

    if(NOT ARGUMENTS_SKIP_RPATH AND NOT ARGUMENTS_KEEP_RPATHS)
        # Parameter has renamed, but we keep the compatibility with old SKIP_RPATH
        conan_set_rpath()
    endif()

    if(NOT ARGUMENTS_SKIP_STD)
        conan_set_std()
    endif()

    if(NOT ARGUMENTS_SKIP_FPIC)
        conan_set_fpic()
    endif()

    conan_check_compiler()
    conan_set_libcxx()
    conan_set_vs_runtime()
    conan_set_find_paths()
    conan_include_build_modules()
    conan_set_find_library_paths()
endmacro()


macro(conan_set_find_paths)
    # CMAKE_MODULE_PATH does not have Debug/Release config, but there are variables
    # CONAN_CMAKE_MODULE_PATH_DEBUG to be used by the consumer
    # CMake can find findXXX.cmake files in the root of packages
    set(CMAKE_MODULE_PATH ${CONAN_CMAKE_MODULE_PATH} ${CMAKE_MODULE_PATH})

    # Make find_package() to work
    set(CMAKE_PREFIX_PATH ${CONAN_CMAKE_MODULE_PATH} ${CMAKE_PREFIX_PATH})

    # Set the find root path (cross build)
    set(CMAKE_FIND_ROOT_PATH ${CONAN_CMAKE_FIND_ROOT_PATH} ${CMAKE_FIND_ROOT_PATH})
    if(CONAN_CMAKE_FIND_ROOT_PATH_MODE_PROGRAM)
        set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM ${CONAN_CMAKE_FIND_ROOT_PATH_MODE_PROGRAM})
    endif()
    if(CONAN_CMAKE_FIND_ROOT_PATH_MODE_LIBRARY)
        set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ${CONAN_CMAKE_FIND_ROOT_PATH_MODE_LIBRARY})
    endif()
    if(CONAN_CMAKE_FIND_ROOT_PATH_MODE_INCLUDE)
        set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ${CONAN_CMAKE_FIND_ROOT_PATH_MODE_INCLUDE})
    endif()
endmacro()


macro(conan_set_find_library_paths)
    # CMAKE_INCLUDE_PATH, CMAKE_LIBRARY_PATH does not have Debug/Release config, but there are variables
    # CONAN_INCLUDE_DIRS_DEBUG/RELEASE CONAN_LIB_DIRS_DEBUG/RELEASE to be used by the consumer
    # For find_library
    set(CMAKE_INCLUDE_PATH ${CONAN_INCLUDE_DIRS} ${CMAKE_INCLUDE_PATH})
    set(CMAKE_LIBRARY_PATH ${CONAN_LIB_DIRS} ${CMAKE_LIBRARY_PATH})
endmacro()


macro(conan_set_vs_runtime)
    if(CONAN_LINK_RUNTIME)
        conan_get_policy(CMP0091 policy_0091)
        if(policy_0091 STREQUAL "NEW")
            if(CONAN_LINK_RUNTIME MATCHES "MTd")
                set(CMAKE_MSVC_RUNTIME_LIBRARY "MultiThreadedDebug")
            elseif(CONAN_LINK_RUNTIME MATCHES "MDd")
                set(CMAKE_MSVC_RUNTIME_LIBRARY "MultiThreadedDebugDLL")
            elseif(CONAN_LINK_RUNTIME MATCHES "MT")
                set(CMAKE_MSVC_RUNTIME_LIBRARY "MultiThreaded")
            elseif(CONAN_LINK_RUNTIME MATCHES "MD")
                set(CMAKE_MSVC_RUNTIME_LIBRARY "MultiThreadedDLL")
            endif()
        else()
            foreach(flag CMAKE_C_FLAGS_RELEASE CMAKE_CXX_FLAGS_RELEASE
                         CMAKE_C_FLAGS_RELWITHDEBINFO CMAKE_CXX_FLAGS_RELWITHDEBINFO
                         CMAKE_C_FLAGS_MINSIZEREL CMAKE_CXX_FLAGS_MINSIZEREL)
                if(DEFINED ${flag})
                    string(REPLACE "/MD" ${CONAN_LINK_RUNTIME} ${flag} "${${flag}}")
                endif()
            endforeach()
            foreach(flag CMAKE_C_FLAGS_DEBUG CMAKE_CXX_FLAGS_DEBUG)
                if(DEFINED ${flag})
                    string(REPLACE "/MDd" ${CONAN_LINK_RUNTIME} ${flag} "${${flag}}")
                endif()
            endforeach()
        endif()
    endif()
endmacro()


macro(conan_flags_setup)
    # Macro maintained for backwards compatibility
    conan_set_find_library_paths()
    conan_global_flags()
    conan_set_rpath()
    conan_set_vs_runtime()
    conan_set_libcxx()
endmacro()


function(conan_message MESSAGE_OUTPUT)
    if(NOT CONAN_CMAKE_SILENT_OUTPUT)
        message(${ARGV${0}})
    endif()
endfunction()


function(conan_get_policy policy_id policy)
    if(POLICY "${policy_id}")
        cmake_policy(GET "${policy_id}" _policy)
        set(${policy} "${_policy}" PARENT_SCOPE)
    else()
        set(${policy} "" PARENT_SCOPE)
    endif()
endfunction()


function(conan_find_libraries_abs_path libraries package_libdir libraries_abs_path)
    foreach(_LIBRARY_NAME ${libraries})
        find_library(CONAN_FOUND_LIBRARY NAME ${_LIBRARY_NAME} PATHS ${package_libdir}
                     NO_DEFAULT_PATH NO_CMAKE_FIND_ROOT_PATH)
        if(CONAN_FOUND_LIBRARY)
            conan_message(STATUS "Library ${_LIBRARY_NAME} found ${CONAN_FOUND_LIBRARY}")
            set(CONAN_FULLPATH_LIBS ${CONAN_FULLPATH_LIBS} ${CONAN_FOUND_LIBRARY})
        else()
            conan_message(STATUS "Library ${_LIBRARY_NAME} not found in package, might be system one")
            set(CONAN_FULLPATH_LIBS ${CONAN_FULLPATH_LIBS} ${_LIBRARY_NAME})
        endif()
        unset(CONAN_FOUND_LIBRARY CACHE)
    endforeach()
    set(${libraries_abs_path} ${CONAN_FULLPATH_LIBS} PARENT_SCOPE)
endfunction()


function(conan_package_library_targets libraries package_libdir libraries_abs_path deps build_type package_name)
    unset(_CONAN_ACTUAL_TARGETS CACHE)
    unset(_CONAN_FOUND_SYSTEM_LIBS CACHE)
    foreach(_LIBRARY_NAME ${libraries})
        find_library(CONAN_FOUND_LIBRARY NAME ${_LIBRARY_NAME} PATHS ${package_libdir}
                     NO_DEFAULT_PATH NO_CMAKE_FIND_ROOT_PATH)
        if(CONAN_FOUND_LIBRARY)
            conan_message(STATUS "Library ${_LIBRARY_NAME} found ${CONAN_FOUND_LIBRARY}")
            set(_LIB_NAME CONAN_LIB::${package_name}_${_LIBRARY_NAME}${build_type})
            add_library(${_LIB_NAME} UNKNOWN IMPORTED)
            set_target_properties(${_LIB_NAME} PROPERTIES IMPORTED_LOCATION ${CONAN_FOUND_LIBRARY})
            set(CONAN_FULLPATH_LIBS ${CONAN_FULLPATH_LIBS} ${_LIB_NAME})
            set(_CONAN_ACTUAL_TARGETS ${_CONAN_ACTUAL_TARGETS} ${_LIB_NAME})
        else()
            conan_message(STATUS "Library ${_LIBRARY_NAME} not found in package, might be system one")
            set(CONAN_FULLPATH_LIBS ${CONAN_FULLPATH_LIBS} ${_LIBRARY_NAME})
            set(_CONAN_FOUND_SYSTEM_LIBS "${_CONAN_FOUND_SYSTEM_LIBS};${_LIBRARY_NAME}")
        endif()
        unset(CONAN_FOUND_LIBRARY CACHE)
    endforeach()

    # Add all dependencies to all targets
    string(REPLACE " " ";" deps_list "${deps}")
    foreach(_CONAN_ACTUAL_TARGET ${_CONAN_ACTUAL_TARGETS})
        set_property(TARGET ${_CONAN_ACTUAL_TARGET} PROPERTY INTERFACE_LINK_LIBRARIES "${_CONAN_FOUND_SYSTEM_LIBS};${deps_list}")
    endforeach()

    set(${libraries_abs_path} ${CONAN_FULLPATH_LIBS} PARENT_SCOPE)
endfunction()


macro(conan_set_libcxx)
    if(DEFINED CONAN_LIBCXX)
        conan_message(STATUS "Conan: C++ stdlib: ${CONAN_LIBCXX}")
        if(CONAN_COMPILER STREQUAL "clang" OR CONAN_COMPILER STREQUAL "apple-clang")
            if(CONAN_LIBCXX STREQUAL "libstdc++" OR CONAN_LIBCXX STREQUAL "libstdc++11" )
                set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -stdlib=libstdc++")
            elseif(CONAN_LIBCXX STREQUAL "libc++")
                set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -stdlib=libc++")
            endif()
        endif()
        if(CONAN_COMPILER STREQUAL "sun-cc")
            if(CONAN_LIBCXX STREQUAL "libCstd")
                set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -library=Cstd")
            elseif(CONAN_LIBCXX STREQUAL "libstdcxx")
                set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -library=stdcxx4")
            elseif(CONAN_LIBCXX STREQUAL "libstlport")
                set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -library=stlport4")
            elseif(CONAN_LIBCXX STREQUAL "libstdc++")
                set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -library=stdcpp")
            endif()
        endif()
        if(CONAN_LIBCXX STREQUAL "libstdc++11")
            add_definitions(-D_GLIBCXX_USE_CXX11_ABI=1)
        elseif(CONAN_LIBCXX STREQUAL "libstdc++")
            add_definitions(-D_GLIBCXX_USE_CXX11_ABI=0)
        endif()
    endif()
endmacro()


macro(conan_set_std)
    conan_message(STATUS "Conan: Adjusting language standard")
    # Do not warn "Manually-specified variables were not used by the project"
    set(ignorevar "${CONAN_STD_CXX_FLAG}${CONAN_CMAKE_CXX_STANDARD}${CONAN_CMAKE_CXX_EXTENSIONS}")
    if (CMAKE_VERSION VERSION_LESS "3.1" OR
        (CMAKE_VERSION VERSION_LESS "3.12" AND ("${CONAN_CMAKE_CXX_STANDARD}" STREQUAL "20" OR "${CONAN_CMAKE_CXX_STANDARD}" STREQUAL "gnu20")))
        if(CONAN_STD_CXX_FLAG)
            conan_message(STATUS "Conan setting CXX_FLAGS flags: ${CONAN_STD_CXX_FLAG}")
            set(CMAKE_CXX_FLAGS "${CONAN_STD_CXX_FLAG} ${CMAKE_CXX_FLAGS}")
        endif()
    else()
        if(CONAN_CMAKE_CXX_STANDARD)
            conan_message(STATUS "Conan setting CPP STANDARD: ${CONAN_CMAKE_CXX_STANDARD} WITH EXTENSIONS ${CONAN_CMAKE_CXX_EXTENSIONS}")
            set(CMAKE_CXX_STANDARD ${CONAN_CMAKE_CXX_STANDARD})
            set(CMAKE_CXX_EXTENSIONS ${CONAN_CMAKE_CXX_EXTENSIONS})
        endif()
    endif()
endmacro()


macro(conan_set_rpath)
    conan_message(STATUS "Conan: Adjusting default RPATHs Conan policies")
    if(APPLE)
        # https://cmake.org/Wiki/CMake_RPATH_handling
        # CONAN GUIDE: All generated libraries should have the id and dependencies to other
        # dylibs without path, just the name, EX:
        # libMyLib1.dylib:
        #     libMyLib1.dylib (compatibility version 0.0.0, current version 0.0.0)
        #     libMyLib0.dylib (compatibility version 0.0.0, current version 0.0.0)
        #     /usr/lib/libc++.1.dylib (compatibility version 1.0.0, current version 120.0.0)
        #     /usr/lib/libSystem.B.dylib (compatibility version 1.0.0, current version 1197.1.1)
        # AVOID RPATH FOR *.dylib, ALL LIBS BETWEEN THEM AND THE EXE
        # SHOULD BE ON THE LINKER RESOLVER PATH (./ IS ONE OF THEM)
        set(CMAKE_SKIP_RPATH 1 CACHE BOOL "rpaths" FORCE)
        # Policy CMP0068
        # We want the old behavior, in CMake >= 3.9 CMAKE_SKIP_RPATH won't affect the install_name in OSX
        set(CMAKE_INSTALL_NAME_DIR "")
    endif()
endmacro()


macro(conan_set_fpic)
    if(DEFINED CONAN_CMAKE_POSITION_INDEPENDENT_CODE)
        conan_message(STATUS "Conan: Adjusting fPIC flag (${CONAN_CMAKE_POSITION_INDEPENDENT_CODE})")
        set(CMAKE_POSITION_INDEPENDENT_CODE ${CONAN_CMAKE_POSITION_INDEPENDENT_CODE})
    endif()
endmacro()


macro(conan_output_dirs_setup)
    set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/bin)
    set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_RELEASE ${CMAKE_RUNTIME_OUTPUT_DIRECTORY})
    set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_RELWITHDEBINFO ${CMAKE_RUNTIME_OUTPUT_DIRECTORY})
    set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_MINSIZEREL ${CMAKE_RUNTIME_OUTPUT_DIRECTORY})
    set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_DEBUG ${CMAKE_RUNTIME_OUTPUT_DIRECTORY})

    set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/lib)
    set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY_RELEASE ${CMAKE_ARCHIVE_OUTPUT_DIRECTORY})
    set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY_RELWITHDEBINFO ${CMAKE_ARCHIVE_OUTPUT_DIRECTORY})
    set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY_MINSIZEREL ${CMAKE_ARCHIVE_OUTPUT_DIRECTORY})
    set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY_DEBUG ${CMAKE_ARCHIVE_OUTPUT_DIRECTORY})

    set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/lib)
    set(CMAKE_LIBRARY_OUTPUT_DIRECTORY_RELEASE ${CMAKE_LIBRARY_OUTPUT_DIRECTORY})
    set(CMAKE_LIBRARY_OUTPUT_DIRECTORY_RELWITHDEBINFO ${CMAKE_LIBRARY_OUTPUT_DIRECTORY})
    set(CMAKE_LIBRARY_OUTPUT_DIRECTORY_MINSIZEREL ${CMAKE_LIBRARY_OUTPUT_DIRECTORY})
    set(CMAKE_LIBRARY_OUTPUT_DIRECTORY_DEBUG ${CMAKE_LIBRARY_OUTPUT_DIRECTORY})
endmacro()


macro(conan_split_version VERSION_STRING MAJOR MINOR)
    #make a list from the version string
    string(REPLACE "." ";" VERSION_LIST "${VERSION_STRING}")

    #write output values
    list(LENGTH VERSION_LIST _version_len)
    list(GET VERSION_LIST 0 ${MAJOR})
    if(${_version_len} GREATER 1)
        list(GET VERSION_LIST 1 ${MINOR})
    endif()
endmacro()


macro(conan_error_compiler_version)
    message(FATAL_ERROR "Detected a mismatch for the compiler version between your conan profile settings and CMake: \n"
                        "Compiler version specified in your conan profile: ${CONAN_COMPILER_VERSION}\n"
                        "Compiler version detected in CMake: ${VERSION_MAJOR}.${VERSION_MINOR}\n"
                        "Please check your conan profile settings (conan profile show [default|your_profile_name])\n"
                        "P.S. You may set CONAN_DISABLE_CHECK_COMPILER CMake variable in order to disable this check."
           )
endmacro()

set(_CONAN_CURRENT_DIR ${CMAKE_CURRENT_LIST_DIR})

function(conan_get_compiler CONAN_INFO_COMPILER CONAN_INFO_COMPILER_VERSION)
    conan_message(STATUS "Current conanbuildinfo.cmake directory: " ${_CONAN_CURRENT_DIR})
    if(NOT EXISTS ${_CONAN_CURRENT_DIR}/conaninfo.txt)
        conan_message(STATUS "WARN: conaninfo.txt not found")
        return()
    endif()

    file (READ "${_CONAN_CURRENT_DIR}/conaninfo.txt" CONANINFO)

    # MATCHALL will match all, including the last one, which is the full_settings one
    string(REGEX MATCH "full_settings.*" _FULL_SETTINGS_MATCHED ${CONANINFO})
    string(REGEX MATCH "compiler=([-A-Za-z0-9_ ]+)" _MATCHED ${_FULL_SETTINGS_MATCHED})
    if(DEFINED CMAKE_MATCH_1)
        string(STRIP "${CMAKE_MATCH_1}" _CONAN_INFO_COMPILER)
        set(${CONAN_INFO_COMPILER} ${_CONAN_INFO_COMPILER} PARENT_SCOPE)
    endif()

    string(REGEX MATCH "compiler.version=([-A-Za-z0-9_.]+)" _MATCHED ${_FULL_SETTINGS_MATCHED})
    if(DEFINED CMAKE_MATCH_1)
        string(STRIP "${CMAKE_MATCH_1}" _CONAN_INFO_COMPILER_VERSION)
        set(${CONAN_INFO_COMPILER_VERSION} ${_CONAN_INFO_COMPILER_VERSION} PARENT_SCOPE)
    endif()
endfunction()


function(check_compiler_version)
    conan_split_version(${CMAKE_CXX_COMPILER_VERSION} VERSION_MAJOR VERSION_MINOR)
    if(DEFINED CONAN_SETTINGS_COMPILER_TOOLSET)
       conan_message(STATUS "Conan: Skipping compiler check: Declared 'compiler.toolset'")
       return()
    endif()
    if(CMAKE_CXX_COMPILER_ID MATCHES MSVC)
        # MSVC_VERSION is defined since 2.8.2 at least
        # https://cmake.org/cmake/help/v2.8.2/cmake.html#variable:MSVC_VERSION
        # https://cmake.org/cmake/help/v3.14/variable/MSVC_VERSION.html
        if(
            # 1920-1929 = VS 16.0 (v142 toolset)
            (CONAN_COMPILER_VERSION STREQUAL "16" AND NOT((MSVC_VERSION GREATER 1919) AND (MSVC_VERSION LESS 1930))) OR
            # 1910-1919 = VS 15.0 (v141 toolset)
            (CONAN_COMPILER_VERSION STREQUAL "15" AND NOT((MSVC_VERSION GREATER 1909) AND (MSVC_VERSION LESS 1920))) OR
            # 1900      = VS 14.0 (v140 toolset)
            (CONAN_COMPILER_VERSION STREQUAL "14" AND NOT(MSVC_VERSION EQUAL 1900)) OR
            # 1800      = VS 12.0 (v120 toolset)
            (CONAN_COMPILER_VERSION STREQUAL "12" AND NOT VERSION_MAJOR STREQUAL "18") OR
            # 1700      = VS 11.0 (v110 toolset)
            (CONAN_COMPILER_VERSION STREQUAL "11" AND NOT VERSION_MAJOR STREQUAL "17") OR
            # 1600      = VS 10.0 (v100 toolset)
            (CONAN_COMPILER_VERSION STREQUAL "10" AND NOT VERSION_MAJOR STREQUAL "16") OR
            # 1500      = VS  9.0 (v90 toolset)
            (CONAN_COMPILER_VERSION STREQUAL "9" AND NOT VERSION_MAJOR STREQUAL "15") OR
            # 1400      = VS  8.0 (v80 toolset)
            (CONAN_COMPILER_VERSION STREQUAL "8" AND NOT VERSION_MAJOR STREQUAL "14") OR
            # 1310      = VS  7.1, 1300      = VS  7.0
            (CONAN_COMPILER_VERSION STREQUAL "7" AND NOT VERSION_MAJOR STREQUAL "13") OR
            # 1200      = VS  6.0
            (CONAN_COMPILER_VERSION STREQUAL "6" AND NOT VERSION_MAJOR STREQUAL "12") )
            conan_error_compiler_version()
        endif()
    elseif(CONAN_COMPILER STREQUAL "gcc")
        conan_split_version(${CONAN_COMPILER_VERSION} CONAN_COMPILER_MAJOR CONAN_COMPILER_MINOR)
        set(_CHECK_VERSION ${VERSION_MAJOR}.${VERSION_MINOR})
        set(_CONAN_VERSION ${CONAN_COMPILER_MAJOR}.${CONAN_COMPILER_MINOR})
        if(NOT ${CONAN_COMPILER_VERSION} VERSION_LESS 5.0)
            conan_message(STATUS "Conan: Compiler GCC>=5, checking major version ${CONAN_COMPILER_VERSION}")
            conan_split_version(${CONAN_COMPILER_VERSION} CONAN_COMPILER_MAJOR CONAN_COMPILER_MINOR)
            if("${CONAN_COMPILER_MINOR}" STREQUAL "")
                set(_CHECK_VERSION ${VERSION_MAJOR})
                set(_CONAN_VERSION ${CONAN_COMPILER_MAJOR})
            endif()
        endif()
        conan_message(STATUS "Conan: Checking correct version: ${_CHECK_VERSION}")
        if(NOT ${_CHECK_VERSION} VERSION_EQUAL ${_CONAN_VERSION})
            conan_error_compiler_version()
        endif()
    elseif(CONAN_COMPILER STREQUAL "clang")
        conan_split_version(${CONAN_COMPILER_VERSION} CONAN_COMPILER_MAJOR CONAN_COMPILER_MINOR)
        set(_CHECK_VERSION ${VERSION_MAJOR}.${VERSION_MINOR})
        set(_CONAN_VERSION ${CONAN_COMPILER_MAJOR}.${CONAN_COMPILER_MINOR})
        if(NOT ${CONAN_COMPILER_VERSION} VERSION_LESS 8.0)
            conan_message(STATUS "Conan: Compiler Clang>=8, checking major version ${CONAN_COMPILER_VERSION}")
            if("${CONAN_COMPILER_MINOR}" STREQUAL "")
                set(_CHECK_VERSION ${VERSION_MAJOR})
                set(_CONAN_VERSION ${CONAN_COMPILER_MAJOR})
            endif()
        endif()
        conan_message(STATUS "Conan: Checking correct version: ${_CHECK_VERSION}")
        if(NOT ${_CHECK_VERSION} VERSION_EQUAL ${_CONAN_VERSION})
            conan_error_compiler_version()
        endif()
    elseif(CONAN_COMPILER STREQUAL "apple-clang" OR CONAN_COMPILER STREQUAL "sun-cc")
        conan_split_version(${CONAN_COMPILER_VERSION} CONAN_COMPILER_MAJOR CONAN_COMPILER_MINOR)
        if(NOT ${VERSION_MAJOR}.${VERSION_MINOR} VERSION_EQUAL ${CONAN_COMPILER_MAJOR}.${CONAN_COMPILER_MINOR})
           conan_error_compiler_version()
        endif()
    elseif(CONAN_COMPILER STREQUAL "intel")
        conan_split_version(${CONAN_COMPILER_VERSION} CONAN_COMPILER_MAJOR CONAN_COMPILER_MINOR)
        if(NOT ${CONAN_COMPILER_VERSION} VERSION_LESS 19.1)
            if(NOT ${VERSION_MAJOR}.${VERSION_MINOR} VERSION_EQUAL ${CONAN_COMPILER_MAJOR}.${CONAN_COMPILER_MINOR})
               conan_error_compiler_version()
            endif()
        else()
            if(NOT ${VERSION_MAJOR} VERSION_EQUAL ${CONAN_COMPILER_MAJOR})
               conan_error_compiler_version()
            endif()
        endif()
    else()
        conan_message(STATUS "WARN: Unknown compiler '${CONAN_COMPILER}', skipping the version check...")
    endif()
endfunction()


function(conan_check_compiler)
    if(CONAN_DISABLE_CHECK_COMPILER)
        conan_message(STATUS "WARN: Disabled conan compiler checks")
        return()
    endif()
    if(NOT DEFINED CMAKE_CXX_COMPILER_ID)
        if(DEFINED CMAKE_C_COMPILER_ID)
            conan_message(STATUS "This project seems to be plain C, using '${CMAKE_C_COMPILER_ID}' compiler")
            set(CMAKE_CXX_COMPILER_ID ${CMAKE_C_COMPILER_ID})
            set(CMAKE_CXX_COMPILER_VERSION ${CMAKE_C_COMPILER_VERSION})
        else()
            message(FATAL_ERROR "This project seems to be plain C, but no compiler defined")
        endif()
    endif()
    if(NOT CMAKE_CXX_COMPILER_ID AND NOT CMAKE_C_COMPILER_ID)
        # This use case happens when compiler is not identified by CMake, but the compilers are there and work
        conan_message(STATUS "*** WARN: CMake was not able to identify a C or C++ compiler ***")
        conan_message(STATUS "*** WARN: Disabling compiler checks. Please make sure your settings match your environment ***")
        return()
    endif()
    if(NOT DEFINED CONAN_COMPILER)
        conan_get_compiler(CONAN_COMPILER CONAN_COMPILER_VERSION)
        if(NOT DEFINED CONAN_COMPILER)
            conan_message(STATUS "WARN: CONAN_COMPILER variable not set, please make sure yourself that "
                          "your compiler and version matches your declared settings")
            return()
        endif()
    endif()

    if(NOT CMAKE_HOST_SYSTEM_NAME STREQUAL ${CMAKE_SYSTEM_NAME})
        set(CROSS_BUILDING 1)
    endif()

    # If using VS, verify toolset
    if (CONAN_COMPILER STREQUAL "Visual Studio")
        if (CONAN_SETTINGS_COMPILER_TOOLSET MATCHES "LLVM" OR
            CONAN_SETTINGS_COMPILER_TOOLSET MATCHES "clang")
            set(EXPECTED_CMAKE_CXX_COMPILER_ID "Clang")
        elseif (CONAN_SETTINGS_COMPILER_TOOLSET MATCHES "Intel")
            set(EXPECTED_CMAKE_CXX_COMPILER_ID "Intel")
        else()
            set(EXPECTED_CMAKE_CXX_COMPILER_ID "MSVC")
        endif()

        if (NOT CMAKE_CXX_COMPILER_ID MATCHES ${EXPECTED_CMAKE_CXX_COMPILER_ID})
            message(FATAL_ERROR "Incorrect '${CONAN_COMPILER}'. Toolset specifies compiler as '${EXPECTED_CMAKE_CXX_COMPILER_ID}' "
                                "but CMake detected '${CMAKE_CXX_COMPILER_ID}'")
        endif()

    # Avoid checks when cross compiling, apple-clang crashes because its APPLE but not apple-clang
    # Actually CMake is detecting "clang" when you are using apple-clang, only if CMP0025 is set to NEW will detect apple-clang
    elseif((CONAN_COMPILER STREQUAL "gcc" AND NOT CMAKE_CXX_COMPILER_ID MATCHES "GNU") OR
        (CONAN_COMPILER STREQUAL "apple-clang" AND NOT CROSS_BUILDING AND (NOT APPLE OR NOT CMAKE_CXX_COMPILER_ID MATCHES "Clang")) OR
        (CONAN_COMPILER STREQUAL "clang" AND NOT CMAKE_CXX_COMPILER_ID MATCHES "Clang") OR
        (CONAN_COMPILER STREQUAL "sun-cc" AND NOT CMAKE_CXX_COMPILER_ID MATCHES "SunPro") )
        message(FATAL_ERROR "Incorrect '${CONAN_COMPILER}', is not the one detected by CMake: '${CMAKE_CXX_COMPILER_ID}'")
    endif()


    if(NOT DEFINED CONAN_COMPILER_VERSION)
        conan_message(STATUS "WARN: CONAN_COMPILER_VERSION variable not set, please make sure yourself "
                             "that your compiler version matches your declared settings")
        return()
    endif()
    check_compiler_version()
endfunction()


macro(conan_set_flags build_type)
    set(CMAKE_CXX_FLAGS${build_type} "${CMAKE_CXX_FLAGS${build_type}} ${CONAN_CXX_FLAGS${build_type}}")
    set(CMAKE_C_FLAGS${build_type} "${CMAKE_C_FLAGS${build_type}} ${CONAN_C_FLAGS${build_type}}")
    set(CMAKE_SHARED_LINKER_FLAGS${build_type} "${CMAKE_SHARED_LINKER_FLAGS${build_type}} ${CONAN_SHARED_LINKER_FLAGS${build_type}}")
    set(CMAKE_EXE_LINKER_FLAGS${build_type} "${CMAKE_EXE_LINKER_FLAGS${build_type}} ${CONAN_EXE_LINKER_FLAGS${build_type}}")
endmacro()


macro(conan_global_flags)
    if(CONAN_SYSTEM_INCLUDES)
        include_directories(SYSTEM ${CONAN_INCLUDE_DIRS}
                                   "$<$<CONFIG:Release>:${CONAN_INCLUDE_DIRS_RELEASE}>"
                                   "$<$<CONFIG:RelWithDebInfo>:${CONAN_INCLUDE_DIRS_RELWITHDEBINFO}>"
                                   "$<$<CONFIG:MinSizeRel>:${CONAN_INCLUDE_DIRS_MINSIZEREL}>"
                                   "$<$<CONFIG:Debug>:${CONAN_INCLUDE_DIRS_DEBUG}>")
    else()
        include_directories(${CONAN_INCLUDE_DIRS}
                            "$<$<CONFIG:Release>:${CONAN_INCLUDE_DIRS_RELEASE}>"
                            "$<$<CONFIG:RelWithDebInfo>:${CONAN_INCLUDE_DIRS_RELWITHDEBINFO}>"
                            "$<$<CONFIG:MinSizeRel>:${CONAN_INCLUDE_DIRS_MINSIZEREL}>"
                            "$<$<CONFIG:Debug>:${CONAN_INCLUDE_DIRS_DEBUG}>")
    endif()

    link_directories(${CONAN_LIB_DIRS})

    conan_find_libraries_abs_path("${CONAN_LIBS_DEBUG}" "${CONAN_LIB_DIRS_DEBUG}"
                                  CONAN_LIBS_DEBUG)
    conan_find_libraries_abs_path("${CONAN_LIBS_RELEASE}" "${CONAN_LIB_DIRS_RELEASE}"
                                  CONAN_LIBS_RELEASE)
    conan_find_libraries_abs_path("${CONAN_LIBS_RELWITHDEBINFO}" "${CONAN_LIB_DIRS_RELWITHDEBINFO}"
                                  CONAN_LIBS_RELWITHDEBINFO)
    conan_find_libraries_abs_path("${CONAN_LIBS_MINSIZEREL}" "${CONAN_LIB_DIRS_MINSIZEREL}"
                                  CONAN_LIBS_MINSIZEREL)

    add_compile_options(${CONAN_DEFINES}
                        "$<$<CONFIG:Debug>:${CONAN_DEFINES_DEBUG}>"
                        "$<$<CONFIG:Release>:${CONAN_DEFINES_RELEASE}>"
                        "$<$<CONFIG:RelWithDebInfo>:${CONAN_DEFINES_RELWITHDEBINFO}>"
                        "$<$<CONFIG:MinSizeRel>:${CONAN_DEFINES_MINSIZEREL}>")

    conan_set_flags("")
    conan_set_flags("_RELEASE")
    conan_set_flags("_DEBUG")

endmacro()


macro(conan_target_link_libraries target)
    if(CONAN_TARGETS)
        target_link_libraries(${target} ${CONAN_TARGETS})
    else()
        target_link_libraries(${target} ${CONAN_LIBS})
        foreach(_LIB ${CONAN_LIBS_RELEASE})
            target_link_libraries(${target} optimized ${_LIB})
        endforeach()
        foreach(_LIB ${CONAN_LIBS_DEBUG})
            target_link_libraries(${target} debug ${_LIB})
        endforeach()
    endif()
endmacro()


macro(conan_include_build_modules)
    if(CMAKE_BUILD_TYPE)
        if(${CMAKE_BUILD_TYPE} MATCHES "Debug")
            set(CONAN_BUILD_MODULES_PATHS ${CONAN_BUILD_MODULES_PATHS_DEBUG} ${CONAN_BUILD_MODULES_PATHS})
        elseif(${CMAKE_BUILD_TYPE} MATCHES "Release")
            set(CONAN_BUILD_MODULES_PATHS ${CONAN_BUILD_MODULES_PATHS_RELEASE} ${CONAN_BUILD_MODULES_PATHS})
        elseif(${CMAKE_BUILD_TYPE} MATCHES "RelWithDebInfo")
            set(CONAN_BUILD_MODULES_PATHS ${CONAN_BUILD_MODULES_PATHS_RELWITHDEBINFO} ${CONAN_BUILD_MODULES_PATHS})
        elseif(${CMAKE_BUILD_TYPE} MATCHES "MinSizeRel")
            set(CONAN_BUILD_MODULES_PATHS ${CONAN_BUILD_MODULES_PATHS_MINSIZEREL} ${CONAN_BUILD_MODULES_PATHS})
        endif()
    endif()

    foreach(_BUILD_MODULE_PATH ${CONAN_BUILD_MODULES_PATHS})
        include(${_BUILD_MODULE_PATH})
    endforeach()
endmacro()


### Definition of user declared vars (user_info) ###

